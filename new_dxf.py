import ezdxf
from ezdxf.addons import Importer

source_dxf = ezdxf.readfile("pv.dxf")

if 'b_test' not in source_dxf.blocks:
    print("Block 'b_test' not defined.")
    exit()

target_dxf = ezdxf.readfile("pv.dxf")

importer = Importer(source_dxf, target_dxf)
importer.import_block('b_test')
importer.finalize()

msp = target_dxf.modelspace()
msp.add_blockref('b_test', insert=(10, 10))
target_dxf.saveas("blockref_tutorials.dxf")
# from ezdxf.addons import iterdxf
#
# doc = iterdxf.opendxf('pvs.dxf')
# line_exporter = doc.export('new_line.dxf')
# text_exporter = doc.export('new_text.dxf')
# polyline_exporter = doc.export('new_polyline.dxf')
# dxf_test = ezdxf.readfile("pvs.dxf")
# msp_test = dxf_test.modelspace()
# msp_test.add_blockref('dxf_test', (2, 1), dxfattribs={
#     'xscale': 1,
#     'yscale': 1,
#     'rotation': 0
# })
# dxf_test.saveas("blockref_tutorialssss.dxf")
# try:
#     for entity in doc.modelspace():
#         if entity.dxftype() == 'LINE':
#             # msp_test.add_blockref('entity', (2, 1), dxfattribs={
#             #     'xscale': 1,
#             #     'yscale': 1,
#             #     'rotation': 0
#             # })
#             line_exporter.write(entity)
#         elif entity.dxftype() == 'TEXT':
#             text_exporter.write(entity)
#         elif entity.dxftype() == 'POLYLINE':
#             polyline_exporter.write(entity)
#     # dxf_test.saveas("blockref_tutorialssss.dxf")
# finally:
#     line_exporter.close()
#     text_exporter.close()
#     polyline_exporter.close()
#     doc.close()