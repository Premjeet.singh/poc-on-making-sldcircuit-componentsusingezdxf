# import ezdxf
# from ezdxf.addons import Importer
#
# source_dxf = ezdxf.readfile("pv.dxf")
#
# if 'b_test' not in source_dxf.blocks:
#     print("Block 'b_test' not defined.")
#     exit()
#
# target_dxf = ezdxf.readfile("pv.dxf")
#
# importer = Importer(source_dxf, target_dxf)
# importer.import_block('b_test')
# importer.finalize()
#
# msp = target_dxf.modelspace()
# msp.add_blockref('b_test', insert=(10, 10))
# target_dxf.saveas("blockref_tutorials.dxf")
# from ezdxf.addons import iterdxf
#
# doc = iterdxf.opendxf('pvs.dxf')
# line_exporter = doc.export('new_line.dxf')
# text_exporter = doc.export('new_text.dxf')
# polyline_exporter = doc.export('new_polyline.dxf')
# dxf_test = ezdxf.readfile("pvs.dxf")
# msp_test = dxf_test.modelspace()
# msp_test.add_blockref('dxf_test', (2, 1), dxfattribs={
#     'xscale': 1,
#     'yscale': 1,
#     'rotation': 0
# })
# dxf_test.saveas("blockref_tutorialssss.dxf")
# try:
#     for entity in doc.modelspace():
#         if entity.dxftype() == 'LINE':
#             # msp_test.add_blockref('entity', (2, 1), dxfattribs={
#             #     'xscale': 1,
#             #     'yscale': 1,
#             #     'rotation': 0
#             # })
#             line_exporter.write(entity)
#         elif entity.dxftype() == 'TEXT':
#             text_exporter.write(entity)
#         elif entity.dxftype() == 'POLYLINE':
#             polyline_exporter.write(entity)
#     # dxf_test.saveas("blockref_tutorialssss.dxf")
# finally:
#     line_exporter.close()
#     text_exporter.close()
#     polyline_exporter.close()
#     doc.close()
#%%
import ezdxf
from ezdxf.addons import Importer
import random
import os
import sys
from ezdxf.addons import r12writer
def get_random_point():
    """Returns random x, y coordinates."""
    x = random.randint(-100, 100)
    y = random.randint(-100, 100)
    return x, y

# dir2save = 'C:/Users/dell/Downloads'
# # fc1name = os.path.join(dir2save,'comp1.dxf')
# pvname = os.path.join(dir2save,'newpanels.dxf')
# #
# # #doc = ezdxf.new('R2010')
# fclist = [pvname]
# labels = ["pv"]
# newdoc = ezdxf.new('R2010')
# for fn,lab in zip(fclist,labels):
#     print("fn",fn,lab)
#     doc = ezdxf.readfile(fn)
#     print("doc",doc)
#     importer = Importer(doc,newdoc)
#     importer.import_modelspace()
#
#     tblock = newdoc.blocks.new(name=lab)
#     ents = doc.modelspace().query('*')
#     print("panels",doc.linetypes)
#     # doc.linetypes('DASHED')
#     for entity in doc.modelspace():
#         print(entity.dxftype())
#     #         if entity.dxftype() == 'LINE':
#     #             # msp_test.add_blockref('entity', (2, 1), dxfattribs={
#     #             #     'xscale': 1,
#     #             #     'yscale': 1,
#     #             #     'rotation': 0
#     #             # })
#
#     importer.import_entities(ents,tblock)
#
#     msp = newdoc.modelspace()
#     placing_points = [get_random_point() for _ in range(5)]
#     print("placing poiints",placing_points)
#     x_list=[(1,2),(2,0),(3,0),(4,0),(5,0)]
#     for point in range(5):
#         # Every flag has a different scaling and a rotation of -15 deg.
#         random_scale = 0.5 + random.random() * 2.0
#         # Add a block reference to the block named 'Comb' at the coordinates 'point'.
#         if lab =='pv':
#             scaler = 50
#         else:
#             scaler=5
#         points =(point,0)
#         print("x,y",points)
#         block_s= msp.add_blockref(lab, points, dxfattribs={
#             'xscale': point+1,
#             'yscale': 0,
#             'rotation':0
#         })
#         print("blocks",block_s)
dir2save = 'C:/Users/dell/Downloads'
# # # # fc1name = os.path.join(dir2save,'comp1.dxf')
pvname = os.path.join(dir2save,'TestCopy.dxf')
# import pdb
# pdb.set_trace();
#doc = ezdxf.new('R2010')
# fclist = [pvname]
pv_labels = [ "pv"]
newdoc = ezdxf.new()
doc = ezdxf.readfile(pvname)
print("*********dc",doc.layout_names())
for block in doc.blocks:
    print("blocksname",block.name)
# msp2=doc.modelspace()
# msp2.add_line((0, 0), (3, 0))
# # Add a horizontal dimension, default dimension style is 'EZDXF'
# dim = msp2.add_linear_dim(base=(3, 2), p1=(0, 0), p2=(3, 0))
# # Necessary second step, to create the BLOCK entity with the dimension geometry.
# # Additional processing of the dimension line could happen between adding and
# # rendering call.
# dim.render()
# for block in doc.blocks:
#     print("*nblovkanme",block.name)
# doc.layers.new(name='MyLines', dxfattribs={'linetype': 'DASHED', 'color': 7})
print("doc",doc)
for block in doc.layout_names():
    print("blocc",block)
    if block == 'Blatt1' :
        print("gekekek")
        importer = Importer(doc, newdoc)
        importer.import_modelspace()

        tblock = newdoc.blocks.new(name=pv_labels[0])
    # msp.query('*[layer=="construction" and linetype!="DASHED"]')
        ents = doc.modelspace().query('*[layout_names!="Blatt1"]')
        print("panels", ents, doc.linetypes)
        importer.import_entities(ents, tblock)

        msp = newdoc.modelspace()
        placing_points = [get_random_point() for _ in range(5)]
        x_count = 0
        y_count = 0
        msp.add_blockref('pv', (0, 0), dxfattribs={
        'xscale': 0.0000000000000005,
        'yscale': 0.00000000000005,
        })
        newdoc.saveas('TestCopy2.dxf')
# importer = Importer(doc,newdoc)
# importer.import_modelspace()
#
# tblock = newdoc.blocks.new(name=pv_labels[0])
# # msp.query('*[layer=="construction" and linetype!="DASHED"]')
# ents = doc.modelspace().query('*')
# print("panels",ents,doc.linetypes)
# importer.import_entities(ents,tblock)
#
# msp = newdoc.modelspace()
# placing_points = [get_random_point() for _ in range(5)]
# x_count = 0
# y_count = 0
# # for x_cords in range(5):
# #     x_count += 1
# #     y_count += 1
# #     point=(x_count,0)
# #     msp.add_blockref('pv_labels', (x_cords,0), dxfattribs={
# #             'xscale': 20,
# #             'yscale': 20,
# #         })
# msp.add_blockref('pv', (2,2), dxfattribs={
#             'xscale': 20,
#             'yscale': 20,
#         })
# # importer.finalize()
# newdoc.saveas('newdoccs.dxf')

# Now read PV
# dir2save = '/Users/vipluv.aga/Code/fhnw/sld'
# blockname = os.path.join(dir2save, 'BRlegend.dxf')
# import pdb
# pdb.set_trace();
# def print_entity(e):
#     print("dftype",e.dxftype())
#     circle = doc.blocks.new(name='CIRCLE')
#     circle.add_circle((e.dxf.start, e.dxf.end), 10, dxfattribs={'color': 5})
#     point =(e.dxf.start,e.dxf.end)
#     blockref = msp.add_blockref('CIRCLE', point, dxfattribs={
#         'xscale': 0.2,
#         'yscale': 0.3,
#         'rotation': 0
#     })
#     print("LINE on layer: %s\n" % e.dxf.layer)
#     print("start point: %s\n" % e.dxf.start)
#     print("end point: %s\n" % e.dxf.end)


# try:
#     doc = ezdxf.readfile(blockname)
# except IOError:
#     print(f'Not a DXF file or a generic I/O error.')
#     sys.exit(1)
# except ezdxf.DXFStructureError:
#     print(f'Invalid or corrupted DXF file.')
#     sys.exit(2)
#
# msp = doc.modelspace()
# msp.add_text("KWxhKSDSDSSSSSSSSSSSSSSSSSSSSSSSSS", dxfattribs={
#     'style': 'LiberationSerif',
#     'color': '3',
#     'height': 5}).set_pos((2, 0.39), align='CENTER')
# mtext = msp.add_mtext('lorem_ipsum', dxfattribs={'style': 'OpenSans'})
# doc.layers.new(name='MyCircles', dxfattribs={'color': 7})
# print("****************",doc.layouts)
# with r12writer(blockname) as dxf:
#     dxf.add_line((0, 0), (100, 23))
#     dxf.add_circle((5,5), radius=20)
#
# def encircle_entity(e):
#     msp.add_text("KWxhKSDSDSSSSSSSSSSSSSSSSSSSSSSSSS", dxfattribs={
#         'style': 'LiberationSerif',
#         'color': '3',
#         'height': 5}).set_pos((2, 0.39), align='CENTER')
#     # circleCenter = e.dxf.insert
#     l=msp.add_circle((0.5, 0.5), radius=5,dxfattribs={'layer': 'MyCircles'})
#     # msp.add_circle(circleCenter, 12, dxfattribs={'layer': 'MyCircles'})
#     print("Circle entity added")
#
# washBasins = msp.query('*[layer=="Diagrama unifilar"]')
# block_layout = doc.blocks.get('Diagrama unifilar')
# print("*************************8",block_layout)
#
# for e in washBasins:
#     encircle_entity(e)
# # new_doc=doc.blocks.new('blocks')
# # msp.add_blockref('blocks', (1,0), dxfattribs={
# #             'xscale': 3,
# #             'yscale': 3,
# #         })
# # lines = msp.query('LINE')
# def print_entity(e):
#     print("dftype",e.dxftype())
#     block_circle=doc.blocks.get('CIRCLE')
#     if not block_circle:
#         circle = doc.blocks.new(name='CIRCLE')
#         circle.add_circle((0,0), 10, dxfattribs={'color': 5})
#     # point =(int(e.dxf.start),int(e.dxf.end))
#     x_pos=5
#     values = {
#         'NAME': 'KWXGDSLDLLLLLLLLLLLLLLLLLLL',
#         'XPOS': x_pos,
#         'YPOS': 0
#     }
#     blockref = msp.add_blockref('CIRCLE', (0,0), dxfattribs={
#         'xscale': 10,
#         'yscale': 20,
#         'rotation': 0
#     })
#     blockref.add_auto_attribs(values)
#     print("blojxjx***************88",blockref.dxf.insert)
#     print("LINE on layer: %s\n" % e.dxf.layer)
#     print("start point: %s\n" % e.dxf.start)
#     print("end point: %s\n" % e.dxf.end)
# for line in msp.query('LINE'):
#     print_entity(line)

# doc.saveas('legendbxxxxxxxx.dxf')



# flag = doc.blocks.new(name='FLAG')

# # Add DXF entities to the block 'FLAG'.
# # The default base point (= insertion point) of the block is (0, 0).
# flag.add_lwpolyline([(0, 0), (0, 5), (4, 3), (0, 3)])  # the flag symbol as 2D polyline
# flag.add_circle((0, 0), .4, dxfattribs={'color': 2})  # mark the base point with a circle
# importer.finalize()
#
# newdoc.saveas("blockref_read_replicates99.dxf")
# from shutil import copyfile
# import ezdxf
#
# ORIGINAL_FILE = 'TrafoBI.dxf'
# FILE_COPY = 'blockref_read_replicate2.dxf'
# #
# # KEEP_LAYERS = {'Layer1', 'Layer2', 'AndSoOn...'}
# # KEEP_LAYERS_LOWER = {layer.lower() for layer in KEEP_LAYERS}
#
# # copy original DXF file
# copyfile(ORIGINAL_FILE, FILE_COPY)
#
# dwg = ezdxf.readfile(FILE_COPY)
# msp = dwg.modelspace()
# # AutoCAD treats layer names case insensitive: 'Test' == 'TEST'
# # but this is maybe not true for all CAD applications.
# # And NEVER delete entities from a collection while iterating.
# # delete_entities = [entity for entity in msp if entity.dxf.layer.lower() not in KEEP_LAYERS_LOWER]
# #
# # for entity in delete_entities:
# #     msp.unlink_entity(entity)
#
# dwg.saveas("modfied.dxf")
import ezdxf
from ezdxf.addons import Importer
#
#
# def merge(source, target):
#     importer = Importer(source, target)
#     # import all entities from source modelspace into target modelspace
#     importer.import_modelspace()
#     # import all required resources and dependencies
#     importer.finalize()
#
#
# base_dxf = ezdxf.readfile('sld.dxf')
#
# for filename in ('TrafoBI.dxf',):
#     merge_dxf = ezdxf.readfile(filename)
#     merge(merge_dxf, base_dxf)
#
# # base_dxf.save()  # to save as file1.dxf
# base_dxf.saveas('mergedss.dxf')
# import ezdxf
# from ezdxf.addons import Importer
#
# sdoc = ezdxf.readfile('TrafoBI.dxf')
# tdoc = ezdxf.new()
# print("*******************************ajaaj",sdoc.blocks)
# if 'Model' not in sdoc.blocks:
#     print("Block 'b_test' not defined.")
#     # exit()
# msp = tdoc.modelspace()
# circle = tdoc.blocks.new(name='CIRCLE')
# circle.add_circle((0,0), 10, dxfattribs={'color': 5})
# for e in sdoc.blocks:
#     blockref = e.add_blockref('CIRCLE', (0, 0), dxfattribs={
#             'xscale': 10,
#             'yscale': 20,
#             'rotation': 0,
#         })
# #
# tdoc.saveas('tdoc.dxf')
# importer = Importer(sdoc, tdoc)
#
# # import all entities from source modelspace into modelspace of the target drawing
# importer.import_modelspace()
#
# # import all paperspace layouts from source drawing
# # importer.import_paperspace_layouts()
#
# # import all CIRCLE and LINE entities from source modelspace into an arbitrary target layout.
# # create target layout
# tblock = tdoc.blocks.new('SOURCE_ENTS')
# # query source entities
# ents = sdoc.modelspace().query('*')
# # import source entities into target block
# importer.import_entities(ents, tblock)
#
# # This is ALWAYS the last & required step, without finalizing the target drawing is maybe invalid!
# # This step imports all additional required table entries and block definitions.
# importer.finalize()
# #
# tdoc.saveas('importedsfs.dxf')
# ORIGINAL_FILE = 'TrafoBI.dxf'
# FILE_COPY = 'blockref_read_replicate2.dxf'
# file='sld.dxf'
# file2='TrafoBI'
# dxf_file1 = os.read(file)
# dxf_file2 = os.read(file2)
# out_file = os.open( "outfile.dxf", os.O_RDWR|os.O_CREAT )
# # out_file = ezdxf.new('R2010')
# out_file.append(dxf_file1)
# out_file.append(dxf_file2)
# # out_file.append(dxf_file3)
#
# out_file.saves('mmmdifedzz.dxf')
# from ezdxf.addons import Importer
# source_dwg = ezdxf.readfile('TrafoBI.dxf')
# target_dwg = ezdxf.readfile('sld.dxf')
# # importer = ezdxf.Importer(source_dwg, target_dwg)
# # importer.import_all()
# # for drawings without block references:
# importer = Importer(source_dwg, target_dwg)
# #
# # # import all entities from source modelspace into modelspace of the target drawing
# importer.import_modelspace()
# importer.import_tables()
# # importer.import_modelspace_entities()
# target_dwg.saveas('dmerged.dxf')
#
# # check drawing, but works just in the upcoming version v0.8.5
# if ezdxf.version < (0, 8, 5):
#     exit()
# auditor = target_dwg.auditor()
# errors = auditor.run()
# if len(errors) == 0:
#     print('Import OK.')
# else:
#     print('Import maybe failed.')
# import ezdxf
# from ezdxf.addons.dxf2code import entities_to_code, block_to_code
#
# doc = ezdxf.readfile('TrafoBI.dxf')
# msp = doc.modelspace()
# source = entities_to_code(msp)
#
# # create source code for a block definition
# # block_source = block_to_code(doc.blocks['MyBlock'])
# #
# # # merge source code objects
# # source.merge(block_source)
#
# with open('source.py', mode='wt') as f:
#     f.write(source.import_str())
#     f.write('\n\n')
#     f.write(source.code_str())
#     f.write('\n')

