# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
# import matplotlib
# import os
# from os import listdir
# import sys
# import subprocess  # open pdf after plot
# from sldcircuit.PVScheme import SchemDraw_PV as schem  # import SchemDraw_PV as schem
# from sldcircuit.PVScheme.SchemDraw_PV import elements_PV as e# import the elements
# # matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
# from matplotlib.backends.backend_ps import FigureCanvasPS
# # matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
import ezdxf
from ezdxf.addons import Importer
import random
import os



def Ec(res,pvdata,invdata,lang):
    values = list(res.values())
    count = 0
    res_data = {}
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        # data = open("language_translation.json").read()
        # lang_json_data = json.loads(data)
        # Electricalscheme = lang_json_data[lang]['electricalscheme']
        # PVmodule = lang_json_data[lang]['pvmodule']
        # InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        # CombinerBox = lang_json_data[lang]['combinerbox']
        # CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        # Inputs = lang_json_data[lang]['inputs']
        # Outputs = lang_json_data[lang]['outputs']
        # V = lang_json_data[lang]['V']
        # A = lang_json_data[lang]['A']
        # In = lang_json_data[lang]['in']
        # Out = lang_json_data[lang]['out']
        # BB = lang_json_data[lang]['BB']
        # pv = lang_json_data[lang]['pv']
        # cabletype = lang_json_data[lang]['cabletype']
        # gridconnection = lang_json_data[lang]['gridconnection']
        # GND = lang_json_data[lang]['GND']
        # InverterNr = lang_json_data[lang]['InverterNr']
        # MaxInputs = lang_json_data[lang]['MaxInputs']
        # UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,inv
        iNb = 0
        inv_list= []

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()
         # fixes the drawing point [0 / 0]. Return with d.pop() below.
        #
        # k = 0
        #
        # # if Mp == 1:
        # #     k = k + 3
        # # elif Mp == 2:
        # #     k = k + 3
        # # else:
        # #     k = k + 10
        # k =k + 10
        # if NumBB == 1:
        #     k = k * 9
        # elif NumBB == 2:
        #     k = (k + 1) * 6
        # else:
        #     k = (k + 1) * 12
        #
        # d.add(e.PV, d='down', xy=[k * l1 + 1, 0],
        #       botlabel='PV module\n' + str(PVName) +
        #                '\n' + str(PVVolt) + 'V')
        # d.add(e.CB, d='down', xy=[k * l1 + 1, -2.5],
        #       botlabel='Combiner Box')
        # d.add(e.INVDCAC, xy=[k * l1 + 1, -5],
        #       botlabel='Inverter DCAC type:\n' + str(InvName))
        # # d.add(e.INVACDC, xy=[k * l1 + 1, -7.5],
        # #       botlabel='Inverter ACDC type:\n' + str(InvName))
        # # d.add(e.CONVDCDC, xy=[k * l1 + 1, -10],
        # #       botlabel='DCDC Converter type:\n' + str(InvName))
        # # d.add(e.BATTERY, xy=[k * l1 + 1, -12.5],
        # #       botlabel='Battery type:\n' + str(BatName))
        # d.add(e.LINE, d='right', xy=[k * l1 - 0.5, -16.5], l=l1,
        #       rgtlabel='Cable type:\n' + str(Cable))
        #
        # d.pop()  # returns the drawing cursor to the last d.push() --> [0 / 0]
        #
        # # -----------------------------------------------------------------------------
        #
        # # Writing the title above the drawing
        #
        # d.push()
        # d.add(e.LABEL, xy=[0, 1], titlabel='Electrical scheme  ' + str(ProjName))
        # d.pop()

        def bb(data_raw,key,count_list,count_parallelss):  # function
            global ir, iNb
            MPPTin = key
            counts = count_parallelss
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                global dots_list
                dots_list=[]

                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0

                def get_random_point():
                    """Returns random x, y coordinates."""
                    x = random.randint(-100, 100)
                    y = random.randint(-100, 100)
                    return x, y
                # newdoc = ezdxf.new('R2010')
                print("dataraw",data_raw)
                dir2save = 'C:/Users/dell/Downloads'
                fc1name = os.path.join(dir2save, 'line.dxf')
                pvname = os.path.join(dir2save, 'panels.dxf')
                #
                # # doc = ezdxf.new('R2010')
                # # fclist = [pvname]
                pv_labels = "pv"
                line_labels = "line"
                #
                newdoc = ezdxf.new('R2010')
                doc = ezdxf.readfile(pvname)
                print("doc", doc)
                importer = Importer(doc, newdoc)
                importer.import_modelspace()

                tblock = newdoc.blocks.new(name=pv_labels)
                ents = doc.modelspace().query('*')
                importer.import_entities(ents, tblock)
                PH = newdoc.blocks.new(name='PH')
                PH.add_line((0,5), (5, 5))
                PH.add_attdef('NAME', (0, 1), dxfattribs={'height': 0.3, 'color': 3})
                PH.add_attdef('NAME2', (0, 1), dxfattribs={'height': 0.3, 'color': 3})
                msp = newdoc.modelspace()
                # PH = newdoc.blocks.new(name='PH')
                # PH.add_line((0, 0), (2, 0))
                for data in data_raw:
                    # x_count += 1
                    # y_count += 1
                    x_count += 1
                    y_count += 1
                    # import pdb;
                    # pdb.set_trace()
                    # dir2save = 'C:/Users/dell/Downloads'
                    # fc1name = os.path.join(dir2save, 'line.dxf')
                    # pvname = os.path.join(dir2save, 'panels.dxf')
                    #
                    # # doc = ezdxf.new('R2010')
                    # fclist = [fc1name, pvname]
                    # labels = ['line' "panels"]
                    # newdoc = ezdxf.new('R2010')
                    # for fn, lab in zip(fclist, labels):
                    #
                    #     print("fn", fn, lab)
                    #     doc = ezdxf.readfile(fn)
                    #     print("doc", doc)
                    #     importer = Importer(doc, newdoc)
                    #     importer.import_modelspace()
                    #
                    #     tblock = newdoc.blocks.new(name=lab)
                    #     ents = doc.modelspace().query('*')
                    #     print("panels", doc.linetypes)
                    #     importer.import_entities(ents, tblock)
                    #     # point=(x_count,y_count)
                    #     msp = newdoc.modelspace()
                    #     placing_points = [get_random_point() for _ in range(5)]
                    #
                    #     for point in placing_points:
                    #         # Every flag has a different scaling and a rotation of -15 deg.
                    #         random_scale = 0.5 + random.random() * 2.0
                    #         # Add a block reference to the block named 'Comb' at the coordinates 'point'.
                    #         # if lab =='pv':
                    #         #     scaler = 50
                    #         # else:
                    #         #     scaler=
                    #         msp.add_blockref(lab, point, dxfattribs={
                    #             'xscale': 50,
                    #             'yscale':20 ,
                    #         })
                    # dir2save = 'C:/Users/dell/Downloads'
                    # fc1name = os.path.join(dir2save, 'line.dxf')
                    # pvname = os.path.join(dir2save, 'newpanels.dxf')
                    # #
                    # # # doc = ezdxf.new('R2010')
                    # # # fclist = [pvname]
                    # pv_labels = "pv"
                    # line_labels="line"
                    # #
                    # newdoc = ezdxf.new('R2010')
                    # doc = ezdxf.readfile(pvname)
                    # print("doc", doc)
                    # importer = Importer(doc, newdoc)
                    # importer.import_modelspace()
                    #
                    # tblock = newdoc.blocks.new(name=pv_labels)
                    # ents = doc.modelspace().query('*')
                    # importer.import_entities(ents, tblock)
                    # msp = newdoc.modelspace()
                    # doc = ezdxf.readfile(pvname)
                    # print("doc", doc)
                    # importer = Importer(doc, newdoc)
                    # importer.import_modelspace()
                    #
                    # tblock = newdoc.blocks.new(name=labels)
                    # ents = doc.modelspace().query('*')
                    # importer.import_entities(ents, tblock)

                    # msp = newdoc.modelspace()
                    # placing_points = [get_random_point() for _ in range(5)]

                    # for point in placing_points:
                    #     # Every flag has a different scaling and a rotation of -15 deg.
                    #     random_scale = 0.5 + random.random() * 2.0
                    #     # Add a block reference to the block named 'Comb' at the coordinates 'point'.
                    #     # if lab =='pv':
                    #     #     scaler = 50
                    #     # else:
                    #     #     scaler=
                    scaler = 50
                    point = (x_count, 0)
                    # msp.add_blockref(labels, point, dxfattribs={
                    #         'xscale': 2,
                    #         'yscale': 2,
                    #         'rotation': 0
                    #     })
                    temp += 1
                    temp6 += 1
                    if temp == count_list[temp2]:
                        temp = 0
                    if temp6 == frequency_count_list[temp7]:
                        temp6 = 0
                    pv_number += 1
                    if temp7 + 1 < len(frequency_count_list):
                        if frequency_count_list[temp7] <= 3 and frequency_count_list[
                            temp7 + 1] > 3:
                            count_parallel = 0
                    if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                        pv_numbers = pv_number - 1
                        count_parallel = 1
                    if num != data and count_parallel >= 3:
                        pv_numbers = pv_number - 1
                        count_parallel = 1
                    if count_parallel <= 3:
                        if data > 0:
                            print("pointss",data,pv_number,x_count)
                            x_pos = x_count
                            values = {
                                'NAME': "Pv" + str(pv_number) + '-' + '1',
                                'XPOS': x_pos,
                                'YPOS': 0
                            }
                            # print("inside",x_count,y_count)
                            # doc = ezdxf.readfile(pvname)
                            # print("doc", doc)
                            # importer = Importer(doc, newdoc)
                            # importer.import_modelspace()
                            #
                            # tblock = newdoc.blocks.new(name=pv_labels)
                            # ents = doc.modelspace().query('*')
                            # importer.import_entities(ents, tblock)
                            # msp = newdoc.modelspace()
                            points=(x_count,0)
                            s=msp.add_blockref(pv_labels,points , dxfattribs={
                                'xscale': x_count,
                                'yscale': 0,
                                'rotation': 0
                            })
                            s.add_auto_attribs(values)
                            print("hiadd",s)
                            # sf = d.add(e.PV, d='down', label=pv + str(pv_number) + '-' + '1')
                            # dots_list.append(sf)
                            # dot = d.add(e.PH)
                            # sd = d.add(e.PV, label=pv + str(pv_number) + '-' + str(data))
                        if count_parallel == 3 and frequency[data] > 3:
                            print("hello*************")
                            # doc = ezdxf.readfile(pvname)
                            # print("doc", doc)
                            # importer = Importer(doc, newdoc)
                            # importer.import_modelspace()

                            tblock = newdoc.blocks.new(name=pv_labels)
                            ents = doc.modelspace().query('*')
                            importer.import_entities(ents, tblock)
                            msp = newdoc.modelspace()
                            x = msp.add_blockref(pv_labels, point, dxfattribs={
                                'xscale': x_count,
                                'yscale': 0,
                                'rotation': 0
                            })
                            # label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                            # d.add(e.LABEL, xy=label.end + [1, 0],
                            #       label=str(frequency[data]) + 'string')
                            # # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                            # ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=1.6)
                            # end_stand.append(ENDPV)
                            # x = d.add(e.LINE)
                            # # d.add(e.LINE, xy=sf.start)
                            # # d.add(e.LINE)
                            # d.add(e.DOT, xy=sf.start + [3.9, 0])
                            # sf = d.add(e.PV, d='down', label=pv + str(pv_number + (frequency[data] - 3)) + '-' + '1')
                            # dot = d.add(e.PH)
                            # sd = d.add(e.PV, label=pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data))
                            if len(data_raw) != (pv_number + frequency_count_list[temp7] - 3):
                                print("hkkkl")
                                # d.add(e.DOT, xy=sf.start + [2.2, 0])
                        if pv_number != len(data_raw):
                            # ENDPV = d.add(e.LINE, d='right')
                            # end_stand.append(ENDPV)
                            if frequency[data] <= 3 and frequency[data] != 1:
                                if pv_number != sum_count_list[temp2]:
                                    print("gg1")
                                    x_count += 0.05
                                    point = (x_count,-0.06)
                                    msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 10,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    # doc = ezdxf.readfile(fc1name)
                                    # print("doc", doc)
                                    # importer = Importer(doc, newdoc)
                                    # importer.import_modelspace()
                                    #
                                    # tblock = newdoc.blocks.new(name=line_labels)
                                    # ents = doc.modelspace().query('*')
                                    # importer.import_entities(ents, tblock)
                                    # msp = newdoc.modelspace()
                                    # x = msp.add_blockref(line_labels, (3,0), dxfattribs={
                                    #     'xscale': x_count,
                                    #     'yscale': 0,
                                    #     'rotation': 0
                                    # })
                                    # ENDPV = d.add(e.LINE, d='right', l=2.2)
                                    # end_stand.append(ENDPV)
                                    # # x = d.add(e.LINE)
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # ENDPV = d.add(e.LINE, d='right', l=2.2)
                                # end_stand.append(ENDPV)
                                # # x = d.add(e.LINE)
                                # d.add(e.DOT, xy=sf.start + [2.2, 0])
                            if frequency[data] == 1:
                                print("ggab")
                                # ENDPV = d.add(e.DOT)
                                # d.add(e.DOT, xy=sf.start + [2.2, 0])

                            # else:
                            #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                            if count_parallel < 3 and frequency[data] > 3:
                                print("gg2")
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 5,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                # ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=2.2)
                                # end_stand.append(ENDPV)
                                # x = d.add(e.LINE)
                                # global dots
                                # d.add(e.DOT, xy=sf.start + [2.2, 0])
                            else:
                                if count_parallel != 3 or frequency[data] <= 3 or (
                                        count_parallel != 3 and frequency[data] > 3):
                                    print("hh")
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])

                            # d.add(e.LINE, xy=sf.start)
                            # d.add(e.LINE)
                        else:
                            print("hi")
                            # sf = d.add(e.PV, d='down', xy=sf.start + [2,0],label='PV' + str(frequency[data]) + '-' + '1')
                            # dot = d.add(e.PH)
                            # sd = d.add(e.PV, xy=sd.start+[2,0],label='PV' + str(frequency[data]) + '-' + str(data))
                            # ENDPV = d.add(e.LINE, d='right')
                            # d.add(e.SEP)
                            # d.add(e.LINE,xy=sf.start)
                            # d.add(e.SEP)
                            # ENDPV = d.add(e.DOT)
                            # end_stand.append(ENDPV)
                    # end_two[data] = ENDPV
                    if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                        # end_point.append(ENDPV)
                        # end_points[data] = ENDPV
                        num = data
                        # temp2 += 1
                    if pv_number == sum_count_list[temp2]:
                        temp2 += 1
                        temp7 += 1
                    count_parallel += 1
                ir = 0
                siNb = iNb + 1
                importer.finalize()

                newdoc.saveas("blockref_read_replicate.dxf")

            # # adding the Combiner Box, the Inverters and the Batteries:
            # if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
            #     # import pdb
            #     # pdb.set_trace();
            #     if Bat == 2:  # if battery per Building Block do this
            #         CBout = 1
            #         d.push()
            #         count = 0
            #         l=len(data_raw)-1
            #         if len(end_point) >=3 and len(data_raw)>3:
            #             count_raw = 5
            #             count_len = 10+(1*l)
            #             count_theta = -30
            #             count_lens = 3+l
            #             count_thetas = 3.6+l
            #         else:
            #             count_raw = 16
            #             count_len = 13
            #             count_theta = 0
            #         # count_raw = 14
            #         for line in end_point:
            #             count += 1
            #             if len(data_raw) > 2 and len(data_raw)!=4and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #                 # if count % 2 == 0:
            #                 #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
            #                 #     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 # else:
            #                 #     if len(end_point) > 1:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down',theta=-90, l=12.6)
            #                 #     else:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) == 4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 else:
            #                     if len(end_point) > 1:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                         d.add(e.LINE, d='down',theta=-90,l=12.6)
            #                     else:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                         d.add(e.LINE, d='down', l=3 * l1)
            #
            #             if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #             if len(data_raw) >= 3 and len(end_point) ==1:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #
            #         for line in end_two:
            #             count += 1
            #             if len(data_raw) == 2 and len(end_two)!=1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=9.3 * l1)
            #                 else:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) == 2 and len(end_two) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
            #             if len(data_raw) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)
            #
            #         d.push()
            #         # d_right =d.add(e.LINE, d='right', l=2 * l1)
            #         # d.add(e.CONVDCDC, d='down',
            #         #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
            #         #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
            #         #       toplabel='DCDC Converter')
            #         # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
            #         d.pop()
            #         inv_start =d.add(e.LINE, d='down', l=3* l1)
            #         # CBout = 1
            #         # d.add(e.CB, d='down', xy=ENDPV.start,
            #         #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
            #         #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
            #         #       toplabel='Combiner Box ' + str(siNb)
            #         #                + '\nInputs: ' + str(Mp)
            #         #                + '\nOutputs: ' + str(CBout))
            #         # d.push()
            #         # d.add(e.LINE, d='left', l=l1)
            #         # d.add(e.CONVDCDC, d='down',
            #         #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
            #         #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
            #         #       toplabel='DCDC Converter')
            #         # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
            #         # d.pop()
            #         # d.add(e.LINE, d='down', l=2 * l1)
            #
            #     else:
            #         # import pdb
            #         # pdb.set_trace();
            #         CBout = 1
            #         d.push()
            #         count = 0
            #         l = len(data_raw) - 1
            #         if len(end_point) >= 3 and len(data_raw) > 3:
            #             count_raw = 5
            #             count_len = 10 + (1 * l)
            #             count_theta = -30
            #             count_lens = 3 + l
            #             count_thetas = 3.6 + l
            #         else:
            #             count_raw = 16
            #             count_len = 13
            #             count_theta = 0
            #         # count_raw = 14
            #         for line in end_point:
            #             count += 1
            #             if len(data_raw) > 2  and len(data_raw)!=4and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #                 # if count % 2 == 0:
            #                 #         d.add(e.LINE, d='left', xy=ends.end, l=l1)
            #                 #         x = d.add(e.LINE, d='down', l=3 * l1)
            #                 # else:
            #                 #     if len(end_point) > 1:
            #                 #             d.add(e.LINE, d='right', xy=ends.end, l=1.5)
            #                 #             d.add(e.LINE, d='down', theta=-90, l=18.6)
            #                 #     else:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) == 4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 else:
            #                     if len(end_point) > 1:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                         d.add(e.LINE, d='down',theta=-90, l=18.6)
            #                     else:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                         d.add(e.LINE, d='down', l=3 * l1)
            #
            #             if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #             if len(data_raw) >= 3 and len(end_point) ==1:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #
            #         for line in end_two:
            #             count += 1
            #             if len(data_raw) == 2 and len(end_two)!=1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=9.3* l1)
            #                 else:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) ==2 and len(end_two)==1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
            #             if len(data_raw) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)
            #
            #         d.push()
            #         # d_right =d.add(e.LINE, d='right', l=2 * l1)
            #         # d.add(e.CONVDCDC, d='down',
            #         #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
            #         #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
            #         #       toplabel='DCDC Converter')
            #         # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
            #         d.pop()
            #         inv_start = d.add(e.LINE, d='down', l=6* l1)
            #         # d.add(e.CB, d='down', xy=ENDPV.start,
            #         #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
            #         #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
            #         #       toplabel='Combiner Box ' + str(siNb)
            #         #                + '\nInputs: ' + str(Mp)
            #         #                + '\nOutputs: ' + str(CBout))
            #     global inv;
            #     inv= d.add(e.INVDCAC, d='down',
            #           smlltlabel=In  + str(VL2) + V + '/ ' + str(AL2) + A,
            #           smlrtlabel=Out + str(VL3) + V + '/ ' + str(AL3) + A,
            #           toplabel=InverterNr  + str(siNb) +
            #                    '\n'+MaxInputs+': ' + str(Nb_Mppt)
            #                    + '\n'+UsedInputs+': ' + str(MPPTin))
            #     d.add(e.LINE, d='right',xy=inv.start+[6.3,-1.2], l=0.5 * l1)
            #     d_end=d.add(e.LINE, d='down',l=0.2)
            #     d.add(e.SOURCE_Testing99, d='down',xy=d_end.start,smlltlabel='#6mm² PVC 70º 750V')
            #     d_final=d.add(e.LINE, d='down',xy=d_end.end,l=0.5)
            #     d.add(e.BAT_CELL, d='down',xy=d_final.end, toplabel='Battery All BB')
            #     inv_list.append(inv)
            #     count = 0
            #     lengths = 19
            #     l5=18
            #     if counts <= 20:
            #         x = -3.2
            #     else:
            #         x = -6.2
            #     y=0
            #     end_points=end_points
            #     dict_lenght = 0
            #     p=0.6
            #     for end in end_points:
            #         dict_lenght +=1
            #         if len(data_raw) > 2and (len(data_raw)!=4 or len(end_point)!=2) and len(end_point) >=2 and (count != len(data_raw)):
            #             if frequency[end]>1:
            #                 d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
            #                 # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
            #             else:
            #                 d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
            #             if dict_lenght !=len(end_points):
            #                 point =d.add(e.LINE,theta=0,xy=d_point.end, tox=inv_start.end + [x, y])
            #                 d.add(e.LINE,d='down',xy=point.end,toy=inv.start-p)
            #         lengths -= 0.5
            #         l5 -=0.2;
            #         x+=0.2
            #         y-=1
            #         p+=0.001
            #     if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys())==1 and MPPTin>1 or (len(frequency.keys())>1 and len(frequency.keys())!=len(count_list)):
            #         lengths = 19
            #         l5 = 18
            #         if counts <= 20:
            #             x = -3.2
            #         else:
            #             x = -6.2
            #         y = 0
            #         end_points = end_points
            #         dict_lenght = 0
            #         p = 0.6
            #         count = 0
            #         dict_lenght = 0
            #         for end in end_point:
            #             dict_lenght += 1
            #             if len(data_raw) >= 1and (len(data_raw)!=4 or len(end_point)!=2) and len(end_point) >= 1 and (count != len(data_raw)):
            #                 d_point = d.add(e.LINE, d='down', xy=end.start, l=lengths)
            #                 if dict_lenght != len(end_point):
            #                     point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
            #                     d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            #             lengths -= 0.5
            #             l5 -= 0.2;
            #             x += 0.2
            #             y -= 1
            #             p += 0.001
            #
            # else:
            #     # import pdb
            #     # pdb.set_trace();
            #     if Bat == 2:  # if battery per BB do this
            #         CBout = 1
            #         direction = 'right'
            #         count = 0
            #         l = len(data_raw) - 1
            #         if len(end_point) >= 3 and len(data_raw) > 3:
            #             count_raw = 5
            #             count_len = 10 + (1 * l)
            #             count_theta = -30
            #             count_lens = 3 + l
            #             count_thetas = 3.6 + l
            #         else:
            #             count_raw = 16
            #             count_len = 13
            #             count_theta = 0
            #         # count_raw = 14
            #         for line in end_point:
            #             count += 1
            #             if len(data_raw) > 2 and len(data_raw)!=4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #                 # if count % 2 == 0:
            #                 #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
            #                 #     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 # else:
            #                 #     if len(end_point) > 1:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', theta=-90, l=12.6)
            #                 #     else:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) == 4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 else:
            #                     if len(end_point) > 1:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                         d.add(e.LINE, d='down', theta=-90, l=18.6)
            #                     else:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                         d.add(e.LINE, d='down', l=3 * l1)
            #
            #             if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #             if len(data_raw) >= 3 and len(end_point) ==1 :
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #         for line in end_two:
            #             count += 1
            #             if len(data_raw) == 2 and len(end_two)!=1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=9.3* l1)
            #                 else:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) == 2 and len(end_two) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
            #             if len(data_raw) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)
            #
            #         d.push()
            #         # d.add(e.LINE, d='right', l=2 * l1)
            #         # d.add(e.CONVDCDC, d='down',
            #         #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
            #         #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
            #         #       toplabel='DCDC Converter')
            #         # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
            #         d.pop()
            #         inv_start = d.add(e.LINE, d='down', l=6 * l1)
            #         # global inv;
            #         inv = d.add(e.INVDCAC,
            #               smlltlabel=In  + str(VL1) +  V+' / ' + str(AL2) + A,
            #               smlrtlabel=Out + str(VL3) + V +'/ ' + str(AL3) + A,
            #               toplabel=InverterNr  + str(siNb) +
            #                        '\n'+MaxInputs+': ' + str(Nb_Mppt)
            #                        + '\n'+UsedInputs+': ' + str(MPPTin))
            #         inv_list.append(inv)
            #         count = 0
            #         lengths = 19
            #         l5 = 18
            #         if counts <= 20:
            #             x = -3.2
            #         else:
            #             x = -6.2
            #         y = 0
            #         end_points = end_points
            #         dict_lenght = 0
            #         p = 0.6
            #         for end in end_points:
            #             dict_lenght += 1
            #             if len(data_raw) > 2and (len(data_raw)!=4 or len(end_point)!=2) and len(end_point) > 2 and (count != len(data_raw)):
            #                 if frequency[end] > 1:
            #                     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
            #                     # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
            #                 else:
            #                     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
            #                 if dict_lenght != len(end_points):
            #                     point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
            #                     d.add(e.LINE, d='down', xy=point.end, toy=inv.start -p)
            #             lengths -= 0.5
            #             l5 -= 0.2
            #             x += 0.2
            #             y -= 1
            #             p += 0.001
            #         if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or \
            #                 (len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
            #             lengths = 19
            #             l5 = 18
            #             if counts <= 20:
            #                 x = -3.2
            #             else:
            #                 x = -6.2
            #             y = 0
            #             end_points = end_points
            #             dict_lenght = 0
            #             p = 0.6
            #             count = 0
            #             dict_lenght = 0
            #             for end in end_point:
            #                 dict_lenght += 1
            #                 if len(data_raw) >= 1 and (len(data_raw) != 4 or len(end_point) != 2) and len(
            #                         end_point) >= 1 and (count != len(data_raw)):
            #                     d_point = d.add(e.LINE, d='down', xy=end.start, l=lengths)
            #                     if dict_lenght != len(end_point):
            #                         point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
            #                         d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            #                 lengths -= 0.5
            #                 l5 -= 0.2;
            #                 x += 0.2
            #                 y -= 1
            #                 p += 0.001
            #
            #     else:  # if NO battery  do this
            #         # import pdb
            #         # pdb.set_trace();
            #         CBout = 1
            #         direction = 'right'
            #         count = 0
            #         l = len(data_raw) - 1
            #         if len(end_point) >= 3 and len(data_raw) > 3:
            #             count_raw = 5
            #             count_len = 10 + (1 * l)
            #             count_theta = -30
            #             count_lens = 3 + l
            #             count_thetas = 3.6 + l
            #         else:
            #             count_raw = 16
            #             count_len = 13
            #             count_theta = 0
            #         # count_raw = 14
            #         for line in end_point:
            #             count += 1
            #             if len(data_raw) > 2 and len(data_raw) !=4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #                 # if count % 2 == 0:
            #                 #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
            #                 #     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 # else:
            #                 #     if len(end_point) > 1:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', theta=-90, l=12.6)
            #                 #     else:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) == 4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 else:
            #                     if len(end_point) > 1:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                         d.add(e.LINE, d='down', theta=-90, l=18.6)
            #                     else:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                         d.add(e.LINE, d='down', l=3 * l1)
            #
            #             if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #             if len(data_raw) >= 3 and len(end_point) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #
            #         for line in end_two:
            #             count += 1
            #             if len(data_raw) == 2 and len(end_two)!=1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=9.3 * l1)
            #                 else:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw)==2 and len(end_two)==1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
            #             if len(data_raw) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)
            #
            #         d.push()
            #         # d.add(e.LINE, d='right', l=2 * l1)
            #         # d.add(e.CONVDCDC, d='down',
            #         #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
            #         #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
            #         #       toplabel='DCDC Converter')
            #         # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
            #         d.pop()
            #         inv_start = d.add(e.LINE, d='down', l=6 * l1)
            #         # global inv;
            #         inv = d.add(e.INVDCAC,
            #                     smlltlabel=In+ str(VL1) + V +'/ ' + str(AL2) +  A,
            #                     smlrtlabel=Out + str(VL3) + V+' / ' + str(AL3) + A,
            #                     toplabel=InverterNr + str(siNb) +
            #                              '\n'+MaxInputs+': ' + str(Nb_Mppt)
            #                              + '\n'+UsedInputs+': ' + str(MPPTin))
            #         inv_list.append(inv)
            #         count = 0
            #         lengths = 19
            #         l5 = 18
            #         if counts<=20:
            #             x = -3.2
            #         else:
            #             x=-6.2
            #         y = 0
            #         end_points = end_points
            #         dict_lenght = 0
            #         p = 0.6
            #         for end in end_points:
            #             dict_lenght += 1
            #             if len(data_raw) > 2and (len(data_raw)!=4 or len(end_point)!=2) and len(end_point) > 2 and (count != len(data_raw)):
            #                 if frequency[end] > 1:
            #                     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
            #                     # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
            #                 else:
            #                     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
            #                 if dict_lenght != len(end_points):
            #                     point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
            #                     d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            #             lengths -= 0.5
            #             l5 -= 0.2
            #             x += 0.2
            #             y -= 1
            #             p += 0.001
            #         if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or\
            #                 (len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
            #             lengths = 19
            #             l5 = 18
            #             if counts <= 20:
            #                 x = -3.2
            #             else:
            #                 x = -6.2
            #             y = 0
            #             end_points = end_points
            #             dict_lenght = 0
            #             p = 0.6
            #             count = 0
            #             dict_lenght = 0
            #             for end in end_point:
            #                 dict_lenght += 1
            #                 if len(data_raw) >= 1 and (len(data_raw) != 4 or len(end_point) != 2) and len(
            #                         end_point) >= 1 and (count != len(data_raw)):
            #                     d_point = d.add(e.LINE, d='down', xy=end.start, l=lengths)
            #                     if dict_lenght != len(end_point):
            #                         point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
            #                         d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            #                 lengths -= 0.5
            #                 l5 -= 0.2;
            #                 x += 0.2
            #                 y -= 1
            #                 p += 0.001
            # print("invlist",inv_list)
            # for inv in inv_list:
            #     d.add(e.LINE, d='right', xy=inv.start + [6.3, -1.2], l=0.5 * l1)
            #     d_end = d.add(e.LINE, d='down', l=0.2)
            #     d.add(e.SOURCE_Testing99, d='down', xy=d_end.start, smlltlabel='#6mm² PVC 70º 750V')
            #     d_final = d.add(e.LINE, d='down', xy=d_end.end, l=0.5)
            #     d.add(e.BAT_CELL, d='down', xy=d_final.end, toplabel='Battery All BB')
            #     d_s=d.add(e.LINE, d='left', xy=inv.start + [0, -2.3], l=5 * l1)
            #     d_circle=d.add(e.SOURCE_Testing100, d='down', xy=d_s.start+[-5.9,0.75], smlltlabel='#6mm² PVC 70º 750V')
            #     d.add(e.LINE, d='down', xy=d_circle.end+[-3,-1], l=1.2)
            #     d_finals=d.add(e.LINE, d='right',l=0.5)
            #     d.add(e.SOURCE_Testing120, d='down',xy=d_finals.end+[0,0.5],smlrtlabel='2#6mm² (F1+F2) + 6mm² PE'+'\n'+'VC 70º 750V' +'\n'+'Eletroduto galvanizado Ø ??')
            #     d.add(e.LINE, d='down', xy=d_s.end , l=3.9 * l1)
            #     d_joint=d.add(e.DOT)
            #     d_resistor=d.add(e.LINE, d='left', xy=d_joint.end, l=1.5 * l1)
            #     resistor=d.add(e.RBOX_Testing, d='down', xy=d_resistor.end,
            #                      smlltlabel='#6mm² PVC 70º 750V')
            #     resistor_line=d.add(e.LINE, d='down',xy=resistor.end, l=1 * l1)
            #     d.add(e.SOURCE_Testing99, d='down', smlltlabel='#6mm² PVC 70º 750V')
            #     d_resistor_final =d.add(e.LINE, d='down',xy=resistor_line.end, l=0.5)
            #     d.add(e.BAT_CELL, d='down', xy=d_resistor_final.end+[0,-0.2], toplabel='Battery All BB')
            #     T_end=d_resistor = d.add(e.VSS, d='down', xy=d_joint.end+[5,0])
            #     d_resistors=d.add(e.LINE,d='right', xy=T_end.end+[0.3,0.2], l=0.5 * l1)
            #     d_measu = d.add(e.Measurement, d='right')
            #     d_stringbox=d.add(e.StringBox, d='down',xy=d_measu.end+[1.5,1.2])
            #     d_resistor = d.add(e.LINE, d='right', xy=d_stringbox.end+[1.5,1.2], l=0.5 * l1)
            #     d_inversor = d.add(e.INVERSOR,xy=d_stringbox.end+[4.5,2.39], d='down')
            #     d_inv_line = d.add(e.LINE, d='right', xy=d_inversor.end + [2, 1.2], l=0.5 * l1)
            #     d_inv = d.add(e.Power_generation, xy=d_inv_line.end + [3, 1.8], d='down')
            #     d_bipolarstart = d.add(e.LINE, d='down', xy=d_joint.end, l=0.5 * l1)
            #     d_bipolarend = d.add(e.Bi_Polar, d='down',xy=d_bipolarstart.end+[-0.5,-0.2])
            #     d_bipolar = d.add(e.LINE, d='down', xy=d_bipolarend.end+[-0.2,0], l=0.5 * l1)
            #     # d_joint2 = d.add(e.DOT)
            #     d_end = d.add(e.LINE, d='down', l=2)
            #     d_source=d.add(e.SOURCE_Testing100, d='down', xy=d_end.start+[3,-3.5], smlltlabel='#6mm² PVC 70º 750V')
            #     # d_final = d.add(e.LINE, d='right', xy=d_source.end, l=0.5)
            #     d.add(e.SOURCE_Testing150, d='down',xy=d_source.end+[-2.7,-0.2])
            #     d_final = d.add(e.LINE, d='down', xy=d_end.end, l=1.5)
            #     d_final = d.add(e.LINE, d='down', xy=d_final.end+[-0,0], l=2.5)
            #     d_joint2=d.add(e.DOT)
            #     d_final = d.add(e.LINE, d='down', xy=d_joint2.end, l=1.5)
            #     d_source = d.add(e.SOURCE_Testing19, d='down',
            #                      smlltlabel='#6mm² PVC 70º 750V')
            #     d_final = d.add(e.LINE, d='down', xy=d_source.end+[-0.7,0], l=1.5)
            #     d_tripolarend = d.add(e.Tri_Polar, d='down',xy=d_final.end+[-0.5,0])
            #     d_bipolar = d.add(e.LINE, d='down', xy=d_tripolarend.end + [-0.2, 0], l=0.5 * l1)
            #     d_tripolarend = d.add(e.KWxh,d='down')
            #     d_final = d.add(e.LINE, d='right', xy=d_tripolarend.end+[0.72,0.8], l=1.5)
            #     d.add(e.SOURCE_Testing99, d='down', smlltlabel='#6mm² PVC 70º 750V')
            #     d_final = d.add(e.LINE, d='down', xy=d_tripolarend.end, l=1.5)
            #     d_source = d.add(e.SOURCE_Testing19, d='down',
            #                      smlltlabel='#6mm² PVC 70º 750V')
            #     d_final = d.add(e.LINE, d='down', xy=d_source.end + [-0.7, 0], l=1.5)
            #     d_arrow=d.add(e.ARROWLINE,d='right',xy=d_final.end+[-0.35,-1])
            #     d_arrow_right=d.add(e.LINE,d='right',xy=d_arrow.end+[0.3,1.2],l=5*l1)
            #     d.add(e.LINE, d='left', xy=d_arrow.end + [0.3, 1.2], l=5 * l1)
            #     d_arrow = d.add(e.ARROWLINE, d='right', xy=d_arrow_right.end + [-1.2, -0.5])
            #     d_arrow = d.add(e.ARROWLINE, d='right',theta=180, xy=d_arrow_right.end + [-0.69, 0.5])
            #     d_line=d.add(e.LINE,d='right',xy=d_joint2.end,l=5*l1)
            #     d_line2 = d.add(e.LINE, d='down', xy=d_line.end, l=0.5 * l1)
            #     d_arrow = d.add(e.ARROWLINE, d='right', xy=d_line2.end + [-0.3, -1])
            #     d_end=d.add(e.solid_line,d='right',xy=d_line2.end+[-3,0.5],l=1)
            #     d.add(e.dashed_line, d='right',  l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d_junction=d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='down',xy=d_junction.end+[0,0], l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d_junction=d.add(e.solid_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='left', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d_junction=d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='up', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d_junction=d.add(e.solid_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='right', xy=d_junction.end + [0, 0], l=1)
            #     d_end = d.add(e.solid_line, d='right', xy=d_tripolarend.end + [-5, 2.5], l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='down', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='left', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='up', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='right', xy=d_junction.end + [0, 0], l=1)
            #     d_end = d.add(e.solid_line, d='right', xy=d_bipolarend.end + [-5, 2.5], l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='down', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='down', l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d.add(e.solid_line, d='down', l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d_junction=d.add(e.solid_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='left', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='up', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d.add(e.solid_line, d='up', l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d.add(e.solid_line, d='up', l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='right', xy=d_junction.end + [0, 0], l=1)













            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = -2.9

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                if raw == 1:
                    # ENDBB = d.add(e.GND, botlabel=GND, l=l1)
                    # d.add(e.DOT)
                    # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel=BB + str(1))
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1])
                    # BB1DOT = d.add(e.DOT)
                else:
                    # ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * 7, 0],
                    #               botlabel=GND, l=l1)
                    # d.add(e.DOT, xy=ENDBB.start)
                    # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                    #       titlabel=BB + str(no))
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2])
        #
        # else:
            # import pdb;
            # # pdb.set_trace()
            # for key in res_data:
            #     raw += 1
            #     no+= 1
            #     if raw == 1:
            #         ENDBB = d.add(e.GND, botlabel=GND, l=l1)
            #         d.add(e.DOT)
            #         d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel=BB+ str(1))
            #         bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1])
            #         BB1DOT = d.add(e.DOT)
            #     else:
            #         ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * 7, 0],
            #                           botlabel=GND, l=l1)
            #         d.add(e.DOT, xy=ENDBB.start)
            #         d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
            #               titlabel=BB + str(no))
            #         bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1])
            #         d.add(e.LINE, xy=inv.end, to=inv_list[0].end, l=l1)
        # # -----------------------------------------------------------------------------
        #

    # -----------------------------------------------------------------------------
Array4SLD = {"String Config": {
        # 2:[[8],[19],[20],[29],[30],[31],[32]],
        3: [[14, 14], [15, 15],[16,16]],
        # 4:[[9,9]],

    }}

PVData = {
    'I_mp_ref': 39,
    'V_mp_ref': 7,
}
INVData = {

}
print(Ec(Array4SLD,PVData,INVData,"en"))
