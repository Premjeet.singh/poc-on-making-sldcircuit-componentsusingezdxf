# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
# import matplotlib
# import os
# from os import listdir
# import sys
# import subprocess  # open pdf after plot
# from sldcircuit.PVScheme import SchemDraw_PV as schem  # import SchemDraw_PV as schem
# from sldcircuit.PVScheme.SchemDraw_PV import elements_PV as e# import the elements
# # matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
# from matplotlib.backends.backend_ps import FigureCanvasPS
# # matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
import ezdxf
from ezdxf.addons import Importer
import random
import os



def Ec(res,pvdata,invdata,lang):
    values = list(res.values())
    count = 0
    res_data = {}
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        # data = open("language_translation.json").read()
        # lang_json_data = json.loads(data)
        # Electricalscheme = lang_json_data[lang]['electricalscheme']
        # PVmodule = lang_json_data[lang]['pvmodule']
        # InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        # CombinerBox = lang_json_data[lang]['combinerbox']
        # CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        # Inputs = lang_json_data[lang]['inputs']
        # Outputs = lang_json_data[lang]['outputs']
        # V = lang_json_data[lang]['V']
        # A = lang_json_data[lang]['A']
        # In = lang_json_data[lang]['in']
        # Out = lang_json_data[lang]['out']
        # BB = lang_json_data[lang]['BB']
        # pv = lang_json_data[lang]['pv']
        # cabletype = lang_json_data[lang]['cabletype']
        # gridconnection = lang_json_data[lang]['gridconnection']
        # GND = lang_json_data[lang]['GND']
        # InverterNr = lang_json_data[lang]['InverterNr']
        # MaxInputs = lang_json_data[lang]['MaxInputs']
        # UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,inv,doc,msp
        iNb = 0
        inv_list= []
        # dir2save = 'C:/Users/dell/Downloads'
        # # fc1name = os.path.join(dir2save,'comp1.dxf')
        # pvname = os.path.join(dir2save,'GND.dxf')
        #
        # # doc = ezdxf.new('R2010')
        # fclist = [pvname]
        # doc = ezdxf.new('R2010')
        # newdoc = ezdxf.readfile(fclist[0])
        # # doc.layers.new(name='MyLines', dxfattribs={'linetype': 'DASHED', 'color': 7})
        # print("doc",doc)
        # importer = Importer(newdoc,doc)
        # importer.import_modelspace()
        #
        # tblock = doc.blocks.new(name='gnd')
        # ents = newdoc.modelspace().query('*')
        # print("panels",doc.linetypes)
        # importer.import_entities(ents,tblock)
        #
        # msp = newdoc.modelspace()
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        bb_label = doc.blocks.new(name='BB_label')
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag.add_attdef('NAME', (-1.2, 2.3), dxfattribs={'height': 0.3, 'color': 3})
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()
         # fixes the drawing point [0 / 0]. Return with d.pop() below.
        #
        # k = 0
        #
        # # if Mp == 1:
        # #     k = k + 3
        # # elif Mp == 2:
        # #     k = k + 3
        # # else:
        # #     k = k + 10
        # k =k + 10
        # if NumBB == 1:
        #     k = k * 9
        # elif NumBB == 2:
        #     k = (k + 1) * 6
        # else:
        #     k = (k + 1) * 12
        #
        # d.add(e.PV, d='down', xy=[k * l1 + 1, 0],
        #       botlabel='PV module\n' + str(PVName) +
        #                '\n' + str(PVVolt) + 'V')
        # d.add(e.CB, d='down', xy=[k * l1 + 1, -2.5],
        #       botlabel='Combiner Box')
        # d.add(e.INVDCAC, xy=[k * l1 + 1, -5],
        #       botlabel='Inverter DCAC type:\n' + str(InvName))
        # # d.add(e.INVACDC, xy=[k * l1 + 1, -7.5],
        # #       botlabel='Inverter ACDC type:\n' + str(InvName))
        # # d.add(e.CONVDCDC, xy=[k * l1 + 1, -10],
        # #       botlabel='DCDC Converter type:\n' + str(InvName))
        # # d.add(e.BATTERY, xy=[k * l1 + 1, -12.5],
        # #       botlabel='Battery type:\n' + str(BatName))
        # d.add(e.LINE, d='right', xy=[k * l1 - 0.5, -16.5], l=l1,
        #       rgtlabel='Cable type:\n' + str(Cable))
        #
        # d.pop()  # returns the drawing cursor to the last d.push() --> [0 / 0]
        #
        # # -----------------------------------------------------------------------------
        #
        # # Writing the title above the drawing
        #
        # d.push()
        # d.add(e.LABEL, xy=[0, 1], titlabel='Electrical scheme  ' + str(ProjName))
        # d.pop()

        def bb(data_raw,key,count_list,count_parallelss,raw,point):  # function
            global ir, iNb
            MPPTin = key
            counts = count_parallelss
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                global dots_list
                dots_list=[]

                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0

                def get_random_point():
                    """Returns random x, y coordinates."""
                    x = random.randint(-100, 100)
                    y = random.randint(-100, 100)
                    return x, y
                # newdoc = ezdxf.new('R2010')
                x_count = 0
                y_count = 0
                print("data ra",data_raw)
                # doc = ezdxf.new('R2010')
                #
                # # Create a block with the name 'FLAG'
                # flag = doc.blocks.new(name='FLAG')
                # flag2 = doc.blocks.new(name='FLAG2')
                # PH = doc.blocks.new(name='PH')
                # PH.add_line((0, 0), (0.17, 0))
                # DASHED = doc.blocks.new(name='DASHED')
                # DASHED.add_line((0.5, 0.5), (0.6, 0.5))
                # DASHED.add_line((0.9, 0.5), (1, 0.5))
                # DASHED.add_line((1.3, 0.5), (1.4, 0.5))
                # DASHED.add_line((1.7, 0.5), (1.8, 0.5))
                # DASHED.add_line((2.1, 0.5), (2.2, 0.5))
                # DASHED.add_line((2.5, 0.5), (2.6, 0.5))
                # DASHED.add_line((2.9, 0.5), (3, 0.5))
                # DASHED.add_line((3.3, 0.5), (3.4, 0.5))
                # DASHED.add_line((3.7, 0.5), (3.8, 0.5))
                # DASHED.add_line((4, 0.5), (4.1, 0.5))
                # GND = doc.blocks.new(name='GND')
                # GND.add_line((0, 2), (0, 2.1))
                # GND.add_line((-0.08, 2), (0.08, 2))
                # GND.add_line((-0.05, 1.99), (0.05, 1.99))
                # GND.add_line((-0.03, 1.98), (0.03, 1.98))
                # PH.add_line((0, 0.4), (0, 0.6))
                # PH.add_line((0, 0.8), (0, 1))
                # PH.add_line((0, 1.2), (0, 1.4))
                points = []

                # Add DXF entities to the block 'FLAG'.
                # The default base point (= insertion point) of the block is (0, 0).
                # points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                # flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
                # flag.add_line((0, 1), (0.3, 0.6))
                # flag.add_line((0.5, 1), (0.3, 0.6))
                # flag.add_line((0.25, -0.2), (0.25, 0))
                # flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
                # flag2.add_line((0, 1), (0.3, 0.6))
                # flag2.add_line((0.5, 1), (0.3, 0.6))
                # flag2.add_line((0.25, -0.2), (0.25, 0))
                # flag2.add_line((0.25, -0.6), (0.25, -0.5))
                # flag2.add_line((0.25, -1), (0.25, -0.9))
                # flag.add_attdef('NAME', (-1.8, 2.5), dxfattribs={'height': 0.3, 'color': 3})
                # flag2.add_attdef('NAME2', (-1.8, -2), dxfattribs={'height': 0.3, 'color': 3})
                # # flag2.add_line((0.25, -1.4), (0.25, -1.3))
                # # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
                # msp = doc.modelspace()
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]

                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        x_count += 0.78
                        y_count += 1
                        temp += 1
                        temp6 += 1
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': "Pv" + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                print("values", values)
                                print("hi")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': "Pv" + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                print("hello*************")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                # label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                # d.add(e.LABEL, xy=label.end + [1, 0],
                                #       label=str(frequency[data]) + 'string')
                                # # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 4.9,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': "Pv" + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': "Pv" + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # # end_stand.append(ENDPV)
                                # if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                                #     ENDPV = d.add(e.LINE, d='right',l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     d.add(e.DOT,xy=sf.start+[2.2,0])
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })

                                # if frequency[data] == 1:
                                #     ENDPV = d.add(e.DOT)
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                    print("hi")

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                # if count_parallel <3 and frequency[data] >3:
                                #     ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     global dots
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # else:
                                #         if count_parallel !=3 or frequency[data]<=3 or (count_parallel!=3 and frequency[data]>3) :
                                #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    print("gg2")
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })

                                else:
                                    if count_parallel != 3 or frequency_count_list[temp7] <= 3 or (
                                            count_parallel != 3 and frequency_count_list[temp7] > 3):
                                        print("hi")

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                print("hih")

                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                print("hello")
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                print("hellllo")
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        x_count += 0.78
                        y_count += 1
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        # print("pvumner", pv_number)
                        # print("frkddkddldldllist", frequency_count_list[temp2])
                        # if num != data and count_parallel > 3:
                        #     pv_numbers = pv_number -1
                        # if num != data and count_parallel >= 3 :
                        #     pv_numbers = pv_number - 1
                        #     count_parallel = 1
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': "Pv" + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                print("values", values)
                                print("hi")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': "Pv" + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                print("hello*************")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                # label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                # d.add(e.LABEL, xy=label.end + [1, 0],
                                #       label=str(frequency[data]) + 'string')
                                # # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 4.9,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': "Pv" + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': "Pv" + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # # end_stand.append(ENDPV)
                                # if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                                #     ENDPV = d.add(e.LINE, d='right',l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     d.add(e.DOT,xy=sf.start+[2.2,0])
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })

                                # if frequency[data] == 1:
                                #     ENDPV = d.add(e.DOT)
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                    print("hi")

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                # if count_parallel <3 and frequency[data] >3:
                                #     ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     global dots
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # else:
                                #         if count_parallel !=3 or frequency[data]<=3 or (count_parallel!=3 and frequency[data]>3) :
                                #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    print("gg2")
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })


                                else:
                                    if count_parallel != 3 or frequency_count_list[temp7] <= 3 or (
                                            count_parallel != 3 and frequency_count_list[temp7] > 3):
                                        print("hh")

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                print("hhhhh")
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                print("hello")
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                print("hellos")
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    print("second")
                    for data in data_raw:
                        # import pdb
                        # pdb.set_trace();
                        temp += 1
                        temp6 += 1
                        # doc = ezdxf.new('R2010')
                        #
                        # # Create a block with the name 'FLAG'
                        # flag = doc.blocks.new(name='FLAG')
                        # flag2 = doc.blocks.new(name='FLAG2')
                        # PH = doc.blocks.new(name='PH')
                        # PH.add_line((0, 0), (0.17, 0))
                        # # PH.add_line((0, 0.4), (0, 0.6))
                        # # PH.add_line((0, 0.8), (0, 1))
                        # # PH.add_line((0, 1.2), (0, 1.4))
                        # points = []
                        #
                        # # Add DXF entities to the block 'FLAG'.
                        # # The default base point (= insertion point) of the block is (0, 0).
                        # points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        # flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
                        # flag.add_line((0, 1), (0.3, 0.6))
                        # flag.add_line((0.5, 1), (0.3, 0.6))
                        # flag.add_line((0.25, -0.2), (0.25, 0))
                        # flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
                        # flag2.add_line((0, 1), (0.3, 0.6))
                        # flag2.add_line((0.5, 1), (0.3, 0.6))
                        # flag2.add_line((0.25, -0.2), (0.25, 0))
                        # flag2.add_line((0.25, -0.6), (0.25, -0.5))
                        # flag2.add_line((0.25, -1), (0.25, -0.9))
                        # # flag2.add_line((0.25, -1.4), (0.25, -1.3))
                        # # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
                        # msp = doc.modelspace()

                        # Get 50 random placing points.
                        placing_points = [get_random_point() for _ in range(50)]
                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 0.78 + point[0]
                            y_count =1 + point[1]
                            x_count += 0.02
                            y_count += 1
                            # x_count += 0.78
                            # y_count += 1 + point[1]
                             # print("points", points)
                        # Every flag has a different scaling and a rotation of -15 deg.
                        random_scale = 0.5 + random.random() * 2.0
                        # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                        # msp.add_blockref('FLAG', point, dxfattribs={
                        #         'xscale': 0.2,
                        #         'yscale': 0.3,
                        #         'rotation': 0
                        #     })
                        # point = (x_count, 0.6)
                        # msp.add_blockref('FLAG2', point, dxfattribs={
                        #         'xscale': 0.2,
                        #         'yscale': 0.3,
                        #         'rotation': 0
                        #     })
                        # x_count += 0.05
                        # point = (x_count, -0.06)
                        # msp.add_blockref('PH', point, dxfattribs={
                        #         'xscale': 5,
                        #         'yscale': 0.1,
                        #         'rotation': 0
                        #     })
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': "Pv" + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                print("values", values)
                                print("hi")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                print("cordinated",blockref.dxf.insert)
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': "Pv" + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                                # sf = d.add(e.PV, d='down', label=pv + str(pv_number) + '-' + '1')
                                # dots_list.append(sf)
                                # dot = d.add(e.PH)
                                # sd = d.add(e.PV, label=pv + str(pv_number) + '-' + str(data))
                            if count_parallel == 3 and frequency[data] > 3:
                                print("hello*************")
                                point = (x_count, 0)
                                print("points", points)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                # label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                # d.add(e.LABEL, xy=label.end + [1, 0],
                                #       label=str(frequency[data]) + 'string')
                                # # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 5,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': "Pv" + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': "Pv" + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                                # ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=1.6)
                                # end_stand.append(ENDPV)
                                # x = d.add(e.LINE)
                                # # d.add(e.LINE, xy=sf.start)
                                # # d.add(e.LINE)
                                # d.add(e.DOT, xy=sf.start + [3.9, 0])
                                # sf = d.add(e.PV, d='down', label=pv + str(pv_number + (frequency[data] - 3)) + '-' + '1')
                                # dot = d.add(e.PH)
                                # sd = d.add(e.PV, label=pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data))
                                if len(data_raw) != (pv_number + frequency_count_list[temp7] - 3):
                                    print("hkkkl")
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])
                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # end_stand.append(ENDPV)
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        print("cords1", ENDPV.dxf.insert)
                                        end_stand.append(ENDPV)
                                        # ENDPV = d.add(e.LINE, d='right', l=2.2)
                                        # end_stand.append(ENDPV)
                                        # # x = d.add(e.LINE)
                                        # d.add(e.DOT, xy=sf.start + [2.2, 0])
                                    # ENDPV = d.add(e.LINE, d='right', l=2.2)
                                    # end_stand.append(ENDPV)
                                    # # x = d.add(e.LINE)
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency[data] == 1:
                                    print("gg")
                                    # ENDPV = d.add(e.DOT)
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                if count_parallel < 3 and frequency[data] > 3:
                                    print("gg2")
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)
                                    print("cords9", ENDPV.dxf.insert)



                                    # ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=2.2)
                                    # end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    # global dots
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])
                                else:
                                    if count_parallel != 3 or frequency[data] <= 3 or (
                                            count_parallel != 3 and frequency[data] > 3):
                                        print("hh")
                                        # x_count += 0.05
                                        # point = (x_count, -0.06)
                                        # msp.add_blockref('PH', point, dxfattribs={
                                        #     'xscale': 5,
                                        #     'yscale': 0.1,
                                        #     'rotation': 0
                                        # })

                                        # d.add(e.DOT, xy=sf.start + [2.2, 0])

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                print("hi")
                                # sf = d.add(e.PV, d='down', xy=sf.start + [2,0],label='PV' + str(frequency[data]) + '-' + '1')
                                # dot = d.add(e.PH)
                                # sd = d.add(e.PV, xy=sd.start+[2,0],label='PV' + str(frequency[data]) + '-' + str(data))
                                # ENDPV = d.add(e.LINE, d='right')
                                # d.add(e.SEP)
                                # d.add(e.LINE,xy=sf.start)
                                # d.add(e.SEP)
                                # ENDPV = d.add(e.DOT)
                                # end_stand.append(ENDPV)
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                # doc.saveas("blockref_read_replicate2.dxf")

            # # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                # import pdb
                # pdb.set_trace();
                print("hello",MPPTin,Mp)
                if Bat == 2:  # if battery per Building Block do this
                    print("hello", MPPTin, Mp)
                #     CBout = 1
                #     d.push()
                #     count = 0
                #     l=len(data_raw)-1
                #     if len(end_point) >=3 and len(data_raw)>3:
                #         count_raw = 5
                #         count_len = 10+(1*l)
                #         count_theta = -30
                #         count_lens = 3+l
                #         count_thetas = 3.6+l
                #     else:
                #         count_raw = 16
                #         count_len = 13
                #         count_theta = 0
                #     # count_raw = 14
                #     for line in end_point:
                #         count += 1
                #         if len(data_raw) > 2 and len(data_raw)!=4and len(end_point) == 2:
                #             ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                #             # if count % 2 == 0:
                #             #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                #             #     x = d.add(e.LINE, d='down', l=3 * l1)
                #             # else:
                #             #     if len(end_point) > 1:
                #             #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                #             #         d.add(e.LINE, d='down',theta=-90, l=12.6)
                #             #     else:
                #             #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                #             #         d.add(e.LINE, d='down', l=3 * l1)
                #         if len(data_raw) == 4 and len(end_point) == 2:
                #             ends = d.add(e.LINE, d='down', xy=line.start, l=1)
                #             if count % 2 == 0:
                #                 d.add(e.LINE, d='left', xy=ends.end, l=1)
                #                 x = d.add(e.LINE, d='down', l=3 * l1)
                #             else:
                #                 if len(end_point) > 1:
                #                     d.add(e.LINE, d='right', xy=ends.end, l=1)
                #                     d.add(e.LINE, d='down',theta=-90,l=12.6)
                #                 else:
                #                     d.add(e.LINE, d='right', xy=ends.end, l=l1)
                #                     d.add(e.LINE, d='down', l=3 * l1)
                #
                #         if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                #             ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                #         if len(data_raw) >= 3 and len(end_point) ==1:
                #             ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                #
                #     for line in end_two:
                #         count += 1
                #         if len(data_raw) == 2 and len(end_two)!=1:
                #             ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                #             if count % 2 == 0:
                #                 d.add(e.LINE, d='right', xy=ends.end, l=1)
                #                 d.add(e.LINE, d='down', l=9.3 * l1)
                #             else:
                #                 d.add(e.LINE, d='left', xy=ends.end, l=1)
                #                 d.add(e.LINE, d='down', l=3 * l1)
                #         if len(data_raw) == 2 and len(end_two) == 1:
                #             ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                #         if len(data_raw) == 1:
                #             ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)
                #
                #     d.push()
                #     # d_right =d.add(e.LINE, d='right', l=2 * l1)
                #     # d.add(e.CONVDCDC, d='down',
                #     #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                #     #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                #     #       toplabel='DCDC Converter')
                #     # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                #     d.pop()
                #     inv_start =d.add(e.LINE, d='down', l=3* l1)
                    # CBout = 1
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                    #                + '\nOutputs: ' + str(CBout))
                    # d.push()
                    # d.add(e.LINE, d='left', l=l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # d.add(e.LINE, d='down', l=2 * l1)

                else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    l = len(data_raw) - 1
                    print("endtwo",end_two)
                    print("endpoint",end_point)
                    print("endpoits",end_points)
                    if len(end_point) >= 3 and len(data_raw) > 3:
                        count_raw = 5
                        count_len = 10 + (1 * l)
                        count_theta = -30
                        count_lens = 3 + l
                        count_thetas = 3.6 + l
                    else:
                        count_raw = 16
                        count_len = 13
                        count_theta = 0
                    # count_raw = 14
                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2  and len(data_raw)!=4and len(end_point) == 2:
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #         d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #         x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #             d.add(e.LINE, d='right', xy=ends.end, l=1.5)
                            #             d.add(e.LINE, d='down', theta=-90, l=18.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            print("hellos")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=1)
                            #         d.add(e.LINE, d='down',theta=-90, l=18.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            print("helloss")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) ==1:
                            print("hellossss***")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_two:
                        print("heloendtwo")
                        # count += 1
                        # if len(data_raw) == 2 and len(end_two)!=1:
                        #     ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                        #     if count % 2 == 0:
                        #         d.add(e.LINE, d='right', xy=ends.end, l=1)
                        #         d.add(e.LINE, d='down', l=9.3* l1)
                        #     else:
                        #         d.add(e.LINE, d='left', xy=ends.end, l=1)
                        #         d.add(e.LINE, d='down', l=3 * l1)
                        # if len(data_raw) ==2 and len(end_two)==1:
                        #     ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        # if len(data_raw) == 1:
                        #     ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    # d.push()
                    # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # inv_start = d.add(e.LINE, d='down', l=6* l1)
                    # point = (0.62, -0.07)
                    point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + 0.2,
                             list(end_points.values())[len(end_points) - 1].dxf.insert[1])
                    line_down = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                        'xscale': 2,
                        'yscale': 5,
                        'rotation': 0
                    })
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                #     #                + '\nOutputs: ' + str(CBout))
                print("last",list(end_points.values())[len(end_points) - 1].dxf.insert[1])
                global inv;
                point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0]+0.2, list(end_points.values())[len(end_points) - 1].dxf.insert[1]-3)
                blockref = msp.add_blockref('INVERTER', point, dxfattribs={
                    'xscale': 2,
                    'yscale': 1.5,
                    'rotation': 0
                })
                # inv= d.add(e.INVDCAC, d='down',
                #       smlltlabel=In  + str(VL2) + V + '/ ' + str(AL2) + A,
                #       smlrtlabel=Out + str(VL3) + V + '/ ' + str(AL3) + A,
                #       toplabel=InverterNr  + str(siNb) +
                #                '\n'+MaxInputs+': ' + str(Nb_Mppt)
                #                + '\n'+UsedInputs+': ' + str(MPPTin))
                # d.add(e.LINE, d='right',xy=inv.start+[6.3,-1.2], l=0.5 * l1)
                # d_end=d.add(e.LINE, d='down',l=0.2)
                # d.add(e.SOURCE_Testing99, d='down',xy=d_end.start,smlltlabel='#6mm² PVC 70º 750V')
                # d_final=d.add(e.LINE, d='down',xy=d_end.end,l=0.5)
                # d.add(e.BAT_CELL, d='down',xy=d_final.end, toplabel='Battery All BB')
                # inv_list.append(inv)
                count = 0
                lengths = 19
                l5=18
                if counts <= 20:
                    x = -3.2
                else:
                    x = -6.2
                y=0
                end_points=end_points
                dict_lenght = 0
                p=0.6
                for end in end_points:
                    dict_lenght +=1
                    if len(data_raw) > 2and (len(data_raw)!=4 or len(end_point)!=2) and len(end_point) >=2 and (count != len(data_raw)):
                        print("invertersss")
                        # if frequency[end]>1:
                        #     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
                        #     # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
                        # else:
                        #     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
                        # if dict_lenght !=len(end_points):
                        #     point =d.add(e.LINE,theta=0,xy=d_point.end, tox=inv_start.end + [x, y])
                        #     d.add(e.LINE,d='down',xy=point.end,toy=inv.start-p)
                    lengths -= 0.5
                    l5 -=0.2;
                    x+=0.2
                    y-=1
                    p+=0.001
            doc.saveas("blockref_read_replicate5.dxf")

                # if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys())==1 and MPPTin>1 or (len(frequency.keys())>1 and len(frequency.keys())!=len(count_list)):
                #     lengths = 19
                #     l5 = 18
                #     if counts <= 20:
                #         x = -3.2
                #     else:
                #         x = -6.2
                #     y = 0
                #     end_points = end_points
                #     dict_lenght = 0
                #     p = 0.6
                #     count = 0
                #     dict_lenght = 0
                #     for end in end_point:
                #         dict_lenght += 1
                #         if len(data_raw) >= 1and (len(data_raw)!=4 or len(end_point)!=2) and len(end_point) >= 1 and (count != len(data_raw)):
                #             d_point = d.add(e.LINE, d='down', xy=end.start, l=lengths)
                #             if dict_lenght != len(end_point):
                #                 point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
                #                 d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
                #         lengths -= 0.5
                #         l5 -= 0.2;
                #         x += 0.2
                #         y -= 1
                #         p += 0.001

            # else:
            #     # import pdb
            #     # pdb.set_trace();
            #     if Bat == 2:  # if battery per BB do this
            #         CBout = 1
            #         direction = 'right'
            #         count = 0
            #         l = len(data_raw) - 1
            #         if len(end_point) >= 3 and len(data_raw) > 3:
            #             count_raw = 5
            #             count_len = 10 + (1 * l)
            #             count_theta = -30
            #             count_lens = 3 + l
            #             count_thetas = 3.6 + l
            #         else:
            #             count_raw = 16
            #             count_len = 13
            #             count_theta = 0
            #         # count_raw = 14
            #         for line in end_point:
            #             count += 1
            #             if len(data_raw) > 2 and len(data_raw)!=4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #                 # if count % 2 == 0:
            #                 #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
            #                 #     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 # else:
            #                 #     if len(end_point) > 1:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', theta=-90, l=12.6)
            #                 #     else:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) == 4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 else:
            #                     if len(end_point) > 1:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                         d.add(e.LINE, d='down', theta=-90, l=18.6)
            #                     else:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                         d.add(e.LINE, d='down', l=3 * l1)
            #
            #             if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #             if len(data_raw) >= 3 and len(end_point) ==1 :
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #         for line in end_two:
            #             count += 1
            #             if len(data_raw) == 2 and len(end_two)!=1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=9.3* l1)
            #                 else:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) == 2 and len(end_two) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
            #             if len(data_raw) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)
            #
            #         d.push()
            #         # d.add(e.LINE, d='right', l=2 * l1)
            #         # d.add(e.CONVDCDC, d='down',
            #         #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
            #         #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
            #         #       toplabel='DCDC Converter')
            #         # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
            #         d.pop()
            #         inv_start = d.add(e.LINE, d='down', l=6 * l1)
            #         # global inv;
            #         inv = d.add(e.INVDCAC,
            #               smlltlabel=In  + str(VL1) +  V+' / ' + str(AL2) + A,
            #               smlrtlabel=Out + str(VL3) + V +'/ ' + str(AL3) + A,
            #               toplabel=InverterNr  + str(siNb) +
            #                        '\n'+MaxInputs+': ' + str(Nb_Mppt)
            #                        + '\n'+UsedInputs+': ' + str(MPPTin))
            #         inv_list.append(inv)
            #         count = 0
            #         lengths = 19
            #         l5 = 18
            #         if counts <= 20:
            #             x = -3.2
            #         else:
            #             x = -6.2
            #         y = 0
            #         end_points = end_points
            #         dict_lenght = 0
            #         p = 0.6
            #         for end in end_points:
            #             dict_lenght += 1
            #             if len(data_raw) > 2and (len(data_raw)!=4 or len(end_point)!=2) and len(end_point) > 2 and (count != len(data_raw)):
            #                 if frequency[end] > 1:
            #                     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
            #                     # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
            #                 else:
            #                     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
            #                 if dict_lenght != len(end_points):
            #                     point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
            #                     d.add(e.LINE, d='down', xy=point.end, toy=inv.start -p)
            #             lengths -= 0.5
            #             l5 -= 0.2
            #             x += 0.2
            #             y -= 1
            #             p += 0.001
            #         if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or \
            #                 (len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
            #             lengths = 19
            #             l5 = 18
            #             if counts <= 20:
            #                 x = -3.2
            #             else:
            #                 x = -6.2
            #             y = 0
            #             end_points = end_points
            #             dict_lenght = 0
            #             p = 0.6
            #             count = 0
            #             dict_lenght = 0
            #             for end in end_point:
            #                 dict_lenght += 1
            #                 if len(data_raw) >= 1 and (len(data_raw) != 4 or len(end_point) != 2) and len(
            #                         end_point) >= 1 and (count != len(data_raw)):
            #                     d_point = d.add(e.LINE, d='down', xy=end.start, l=lengths)
            #                     if dict_lenght != len(end_point):
            #                         point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
            #                         d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            #                 lengths -= 0.5
            #                 l5 -= 0.2;
            #                 x += 0.2
            #                 y -= 1
            #                 p += 0.001
            #
            #     else:  # if NO battery  do this
            #         # import pdb
            #         # pdb.set_trace();
            #         CBout = 1
            #         direction = 'right'
            #         count = 0
            #         l = len(data_raw) - 1
            #         if len(end_point) >= 3 and len(data_raw) > 3:
            #             count_raw = 5
            #             count_len = 10 + (1 * l)
            #             count_theta = -30
            #             count_lens = 3 + l
            #             count_thetas = 3.6 + l
            #         else:
            #             count_raw = 16
            #             count_len = 13
            #             count_theta = 0
            #         # count_raw = 14
            #         for line in end_point:
            #             count += 1
            #             if len(data_raw) > 2 and len(data_raw) !=4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #                 # if count % 2 == 0:
            #                 #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
            #                 #     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 # else:
            #                 #     if len(end_point) > 1:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', theta=-90, l=12.6)
            #                 #     else:
            #                 #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                 #         d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw) == 4 and len(end_point) == 2:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     x = d.add(e.LINE, d='down', l=3 * l1)
            #                 else:
            #                     if len(end_point) > 1:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                         d.add(e.LINE, d='down', theta=-90, l=18.6)
            #                     else:
            #                         d.add(e.LINE, d='right', xy=ends.end, l=l1)
            #                         d.add(e.LINE, d='down', l=3 * l1)
            #
            #             if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #             if len(data_raw) >= 3 and len(end_point) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=line.start, l=8)
            #
            #         for line in end_two:
            #             count += 1
            #             if len(data_raw) == 2 and len(end_two)!=1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
            #                 if count % 2 == 0:
            #                     d.add(e.LINE, d='right', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=9.3 * l1)
            #                 else:
            #                     d.add(e.LINE, d='left', xy=ends.end, l=1)
            #                     d.add(e.LINE, d='down', l=3 * l1)
            #             if len(data_raw)==2 and len(end_two)==1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
            #             if len(data_raw) == 1:
            #                 ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)
            #
            #         d.push()
            #         # d.add(e.LINE, d='right', l=2 * l1)
            #         # d.add(e.CONVDCDC, d='down',
            #         #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
            #         #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
            #         #       toplabel='DCDC Converter')
            #         # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
            #         d.pop()
            #         inv_start = d.add(e.LINE, d='down', l=6 * l1)
            #         # global inv;
            #         inv = d.add(e.INVDCAC,
            #                     smlltlabel=In+ str(VL1) + V +'/ ' + str(AL2) +  A,
            #                     smlrtlabel=Out + str(VL3) + V+' / ' + str(AL3) + A,
            #                     toplabel=InverterNr + str(siNb) +
            #                              '\n'+MaxInputs+': ' + str(Nb_Mppt)
            #                              + '\n'+UsedInputs+': ' + str(MPPTin))
            #         inv_list.append(inv)
            #         count = 0
            #         lengths = 19
            #         l5 = 18
            #         if counts<=20:
            #             x = -3.2
            #         else:
            #             x=-6.2
            #         y = 0
            #         end_points = end_points
            #         dict_lenght = 0
            #         p = 0.6
            #         for end in end_points:
            #             dict_lenght += 1
            #             if len(data_raw) > 2and (len(data_raw)!=4 or len(end_point)!=2) and len(end_point) > 2 and (count != len(data_raw)):
            #                 if frequency[end] > 1:
            #                     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
            #                     # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
            #                 else:
            #                     d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
            #                 if dict_lenght != len(end_points):
            #                     point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
            #                     d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            #             lengths -= 0.5
            #             l5 -= 0.2
            #             x += 0.2
            #             y -= 1
            #             p += 0.001
            #         if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or\
            #                 (len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
            #             lengths = 19
            #             l5 = 18
            #             if counts <= 20:
            #                 x = -3.2
            #             else:
            #                 x = -6.2
            #             y = 0
            #             end_points = end_points
            #             dict_lenght = 0
            #             p = 0.6
            #             count = 0
            #             dict_lenght = 0
            #             for end in end_point:
            #                 dict_lenght += 1
            #                 if len(data_raw) >= 1 and (len(data_raw) != 4 or len(end_point) != 2) and len(
            #                         end_point) >= 1 and (count != len(data_raw)):
            #                     d_point = d.add(e.LINE, d='down', xy=end.start, l=lengths)
            #                     if dict_lenght != len(end_point):
            #                         point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
            #                         d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            #                 lengths -= 0.5
            #                 l5 -= 0.2;
            #                 x += 0.2
            #                 y -= 1
            #                 p += 0.001
            # print("invlist",inv_list)
            # for inv in inv_list:
            #     d.add(e.LINE, d='right', xy=inv.start + [6.3, -1.2], l=0.5 * l1)
            #     d_end = d.add(e.LINE, d='down', l=0.2)
            #     d.add(e.SOURCE_Testing99, d='down', xy=d_end.start, smlltlabel='#6mm² PVC 70º 750V')
            #     d_final = d.add(e.LINE, d='down', xy=d_end.end, l=0.5)
            #     d.add(e.BAT_CELL, d='down', xy=d_final.end, toplabel='Battery All BB')
            #     d_s=d.add(e.LINE, d='left', xy=inv.start + [0, -2.3], l=5 * l1)
            #     d_circle=d.add(e.SOURCE_Testing100, d='down', xy=d_s.start+[-5.9,0.75], smlltlabel='#6mm² PVC 70º 750V')
            #     d.add(e.LINE, d='down', xy=d_circle.end+[-3,-1], l=1.2)
            #     d_finals=d.add(e.LINE, d='right',l=0.5)
            #     d.add(e.SOURCE_Testing120, d='down',xy=d_finals.end+[0,0.5],smlrtlabel='2#6mm² (F1+F2) + 6mm² PE'+'\n'+'VC 70º 750V' +'\n'+'Eletroduto galvanizado Ø ??')
            #     d.add(e.LINE, d='down', xy=d_s.end , l=3.9 * l1)
            #     d_joint=d.add(e.DOT)
            #     d_resistor=d.add(e.LINE, d='left', xy=d_joint.end, l=1.5 * l1)
            #     resistor=d.add(e.RBOX_Testing, d='down', xy=d_resistor.end,
            #                      smlltlabel='#6mm² PVC 70º 750V')
            #     resistor_line=d.add(e.LINE, d='down',xy=resistor.end, l=1 * l1)
            #     d.add(e.SOURCE_Testing99, d='down', smlltlabel='#6mm² PVC 70º 750V')
            #     d_resistor_final =d.add(e.LINE, d='down',xy=resistor_line.end, l=0.5)
            #     d.add(e.BAT_CELL, d='down', xy=d_resistor_final.end+[0,-0.2], toplabel='Battery All BB')
            #     T_end=d_resistor = d.add(e.VSS, d='down', xy=d_joint.end+[5,0])
            #     d_resistors=d.add(e.LINE,d='right', xy=T_end.end+[0.3,0.2], l=0.5 * l1)
            #     d_measu = d.add(e.Measurement, d='right')
            #     d_stringbox=d.add(e.StringBox, d='down',xy=d_measu.end+[1.5,1.2])
            #     d_resistor = d.add(e.LINE, d='right', xy=d_stringbox.end+[1.5,1.2], l=0.5 * l1)
            #     d_inversor = d.add(e.INVERSOR,xy=d_stringbox.end+[4.5,2.39], d='down')
            #     d_inv_line = d.add(e.LINE, d='right', xy=d_inversor.end + [2, 1.2], l=0.5 * l1)
            #     d_inv = d.add(e.Power_generation, xy=d_inv_line.end + [3, 1.8], d='down')
            #     d_bipolarstart = d.add(e.LINE, d='down', xy=d_joint.end, l=0.5 * l1)
            #     d_bipolarend = d.add(e.Bi_Polar, d='down',xy=d_bipolarstart.end+[-0.5,-0.2])
            #     d_bipolar = d.add(e.LINE, d='down', xy=d_bipolarend.end+[-0.2,0], l=0.5 * l1)
            #     # d_joint2 = d.add(e.DOT)
            #     d_end = d.add(e.LINE, d='down', l=2)
            #     d_source=d.add(e.SOURCE_Testing100, d='down', xy=d_end.start+[3,-3.5], smlltlabel='#6mm² PVC 70º 750V')
            #     # d_final = d.add(e.LINE, d='right', xy=d_source.end, l=0.5)
            #     d.add(e.SOURCE_Testing150, d='down',xy=d_source.end+[-2.7,-0.2])
            #     d_final = d.add(e.LINE, d='down', xy=d_end.end, l=1.5)
            #     d_final = d.add(e.LINE, d='down', xy=d_final.end+[-0,0], l=2.5)
            #     d_joint2=d.add(e.DOT)
            #     d_final = d.add(e.LINE, d='down', xy=d_joint2.end, l=1.5)
            #     d_source = d.add(e.SOURCE_Testing19, d='down',
            #                      smlltlabel='#6mm² PVC 70º 750V')
            #     d_final = d.add(e.LINE, d='down', xy=d_source.end+[-0.7,0], l=1.5)
            #     d_tripolarend = d.add(e.Tri_Polar, d='down',xy=d_final.end+[-0.5,0])
            #     d_bipolar = d.add(e.LINE, d='down', xy=d_tripolarend.end + [-0.2, 0], l=0.5 * l1)
            #     d_tripolarend = d.add(e.KWxh,d='down')
            #     d_final = d.add(e.LINE, d='right', xy=d_tripolarend.end+[0.72,0.8], l=1.5)
            #     d.add(e.SOURCE_Testing99, d='down', smlltlabel='#6mm² PVC 70º 750V')
            #     d_final = d.add(e.LINE, d='down', xy=d_tripolarend.end, l=1.5)
            #     d_source = d.add(e.SOURCE_Testing19, d='down',
            #                      smlltlabel='#6mm² PVC 70º 750V')
            #     d_final = d.add(e.LINE, d='down', xy=d_source.end + [-0.7, 0], l=1.5)
            #     d_arrow=d.add(e.ARROWLINE,d='right',xy=d_final.end+[-0.35,-1])
            #     d_arrow_right=d.add(e.LINE,d='right',xy=d_arrow.end+[0.3,1.2],l=5*l1)
            #     d.add(e.LINE, d='left', xy=d_arrow.end + [0.3, 1.2], l=5 * l1)
            #     d_arrow = d.add(e.ARROWLINE, d='right', xy=d_arrow_right.end + [-1.2, -0.5])
            #     d_arrow = d.add(e.ARROWLINE, d='right',theta=180, xy=d_arrow_right.end + [-0.69, 0.5])
            #     d_line=d.add(e.LINE,d='right',xy=d_joint2.end,l=5*l1)
            #     d_line2 = d.add(e.LINE, d='down', xy=d_line.end, l=0.5 * l1)
            #     d_arrow = d.add(e.ARROWLINE, d='right', xy=d_line2.end + [-0.3, -1])
            #     d_end=d.add(e.solid_line,d='right',xy=d_line2.end+[-3,0.5],l=1)
            #     d.add(e.dashed_line, d='right',  l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d_junction=d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='down',xy=d_junction.end+[0,0], l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d_junction=d.add(e.solid_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='left', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d_junction=d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='up', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d_junction=d.add(e.solid_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='right', xy=d_junction.end + [0, 0], l=1)
            #     d_end = d.add(e.solid_line, d='right', xy=d_tripolarend.end + [-5, 2.5], l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='down', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='left', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='up', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='right', xy=d_junction.end + [0, 0], l=1)
            #     d_end = d.add(e.solid_line, d='right', xy=d_bipolarend.end + [-5, 2.5], l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d.add(e.dashed_line, d='right', l=1)
            #     d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='right', l=1)
            #     d_junction = d.add(e.solid_line, d='down', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='down', l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d.add(e.solid_line, d='down', l=1)
            #     d.add(e.dashed_line, d='down', l=1)
            #     d_junction=d.add(e.solid_line, d='down', l=1)
            #     d_junction = d.add(e.solid_line, d='left', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d.add(e.dashed_line, d='left', l=1)
            #     d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='left', l=1)
            #     d_junction = d.add(e.solid_line, d='up', xy=d_junction.end + [0, 0], l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d.add(e.solid_line, d='up', l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d.add(e.solid_line, d='up', l=1)
            #     d.add(e.dashed_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='up', l=1)
            #     d_junction = d.add(e.solid_line, d='right', xy=d_junction.end + [0, 0], l=1)













            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = -2.9

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        # GND = doc.blocks.new(name='GND')
        # GND.add_line((0, 2), (0, 2.1))
        # GND.add_line((-0.08, 2), (0.08, 2))
        # GND.add_line((-0.05, 1.99), (0.05, 1.99))
        # GND.add_line((-0.03, 1.98), (0.03, 1.98))
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                if raw == 1:
                    # ENDBB = d.add(e.GND, botlabel=GND, l=l1)
                    # d.add(e.DOT)
                    # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel=BB + str(1))
                    point=(0.62,0.98)
                    blockref = msp.add_blockref('LINE', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    point = (0.62, -0.07)
                    blockref = msp.add_blockref('GND', point, dxfattribs={
                        'xscale': 0.5,
                        'yscale': 0.5,
                        'rotation': 0
                    })
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],raw,point)
                    # BB1DOT = d.add(e.DOT)
                else:
                    # ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * 7, 0],
                    #               botlabel=GND, l=l1)
                    # d.add(e.DOT, xy=ENDBB.start)
                    # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                    #       titlabel=BB + str(no))
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],raw,point)
        #
        else:
            import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                if raw == 1:
                    values = {
                        'bb_label': "BB" + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (0.35, 0.9)
                    bb_label = msp.add_blockref('BB_Label', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    bb_label.add_auto_attribs(values)
                    point = (0.62, 0.98)
                    blockref = msp.add_blockref('LINE', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    point = (0.62, -0.07)
                    blockref = msp.add_blockref('GND', point, dxfattribs={
                        'xscale': 0.5,
                        'yscale': 0.5,
                        'rotation': 0
                    })
                    # ENDBB = d.add(e.GND, botlabel=GND, l=l1)
                    # d.add(e.DOT)
                    # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel=BB+ str(1))
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],raw,point)
                    # BB1DOT = d.add(e.DOT)
                else:
                    values = {
                        'bb_label': "BB" + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (0.35, 0.9)
                    bb_label = msp.add_blockref('BB_Label', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    bb_label.add_auto_attribs(values)
                    point = (2, 0.98)+dots_list[-1]
                    print("dotslist",dots_list[-1])
                    print("pinddndnd",point[0])
                    gnd_points = (point[0]+0.65 ,0.98)
                    blockref = msp.add_blockref('LINE', gnd_points, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    point = (2, -0.07)+dots_list[-1]
                    gnd_points = (point[0] + 0.65, -0.065)
                    blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                        'xscale': 0.5,
                        'yscale': 0.5,
                        'rotation': 0
                    })
                    # ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * 7, 0],
                    #                   botlabel=GND, l=l1)
                    # d.add(e.DOT, xy=ENDBB.start)
                    # d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                    #       titlabel=BB + str(no))
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],raw,point)
                    # d.add(e.LINE, xy=inv.end, to=inv_list[0].end, l=l1)
        # # -----------------------------------------------------------------------------
        #

    # -----------------------------------------------------------------------------
Array4SLD = {"String Config": {
        # 2:[[8],[19],[20],[29],[30],[31],[32]],
        3: [[14, 14], [18,18,18],[5,5],[6,6,6]],
        4:[[9,9],[10,10]],
    # 6:[[9,9],[8]]

    }}

PVData = {
    'I_mp_ref': 39,
    'V_mp_ref': 7,
}
INVData = {

}
print(Ec(Array4SLD,PVData,INVData,"en"))
