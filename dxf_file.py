import sys
import ezdxf
import random  # needed for random placing points


def get_random_point():
    """Returns random x, y coordinates."""
    x = random.randint(-100, 100)
    y = random.randint(-100, 100)
    return x, y


# try:
#     doc = ezdxf.readfile("pvs.dxf")
#     # doc_inverter = ezdxf.readfile("inverter.dxf")
#     # doc_combinerbox = ezdxf.readfile("combinerbox.dxf")
#     # doc_cable = ezdxf.readfile("cable.dxf")
#     # doc_grid = ezdxf.readfile("grid.dxf")
#     # doc_line = ezdxf.readfile("line.dxf")
# except IOError:
#     print(f'Not a DXF file or a generic I/O error.')
#     sys.exit(1)
# except ezdxf.DXFStructureError:
#     print(f'Invalid or corrupted DXF file.')
#     sys.exit(2)
# msp = doc.modelspace()
# lines = msp.query('LINE')
# print(msp)
# print(lines)
# msp.add_line((0, 1), (2, 2))  # add a LINE entity
# msp.add_line((0, 1), (0.5, 1.3))
# msp.add_line((0.1, 1), (0.6, 1.3))
# doc.saveas('linesssss.dxf')
dxf = ezdxf.readfile("pvs.dxf")
block_test = dxf.blocks.get('b_test')
# print(dxf.blocks.get('b_test'))
dxf_test = ezdxf.readfile("pvs.dxf")
def print_entity(e):
    print("LINE on layer: %s\n" % e.dxf.layer)
    print("start point: %s\n" % e.dxf.start)
    print("end point: %s\n" % e.dxf.end)


msp_test = dxf_test.modelspace()
msp_test.add_line((0, 2), (5, 2), dxfattribs={'layer': 'MyLines'})
msp_test.add_line((5, 2), (5, 5), dxfattribs={'layer': 'MyLines2'})
for d in msp_test:
    if d.dxftype() == 'LINE':
        print(d)
        print_entity(d)
    else:
        print("otther entoyy",d)
flag = dxf_test.blocks.new(name='FLAG')
# points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
# flag.add_lwpolyline(points)
# flag.add_line((0, 1), (0.3, 0.6))
# flag.add_line((0.5, 1), (0.3, 0.6))
flag.add_lwpolyline([(0, 0), (0, 5), (4, 3), (0, 3)])
flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
# msp_test.add_blockref('block_test', (3, 1), dxfattribs={
# 'xscale': 1,
# 'yscale': 1,
# 'rotation': 0
# })
placing_points = [get_random_point() for _ in range(50)]

for point in placing_points:
    # Every flag has a different scaling and a rotation of -15 deg.
    random_scale = 0.5 + random.random() * 2.0
    # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
    msp_test.add_blockref('FLAG', point, dxfattribs={
        'xscale': random_scale,
        'yscale': random_scale,
        'rotation': -15
    })
# msp_test.add_blockref('flag', (2, 1), dxfattribs={
# 'xscale': 2.1,
# 'yscale': 2.1,
# 'rotation': 115
# })

dxf_test.saveas("blockref_tutorial.dxf")
exit()