import ezdxf
# import random  # needed for random placing points
#
#
# def get_random_point():
#     """Returns random x, y coordinates."""
#     x = random.randint(-100, 100)
#     y = random.randint(-100, 100)
#     return x, y
#
#
# # Create a new drawing in the DXF format of AutoCAD 2010
# doc = ezdxf.new('R2010')
#
# # Create a block with the name 'FLAG'
# flag = doc.blocks.new(name='FLAG')
#
# # Add DXF entities to the block 'FLAG'.
# # The default base point (= insertion point) of the block is (0, 0).
# flag.add_lwpolyline([(0, 0), (0, 5), (4, 3), (0, 3)])  # the flag symbol as 2D polyline
# flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
# doc = ezdxf.new('R2000')
# msp = doc.modelspace()
#
# points = [(1, 3), (1 ,1), (1, 1), (1,3)]
# msp.add_lwpolyline(points)
# points = [(0, 0, 0, .05), (3, 0, .1, .2, -.5), (6, 0, .1, .05), (9, 0)]
# msp.add_lwpolyline(points)
# msp.add_lwpolyline([(0, 0, 0), (10, 0, 1), (20, 0, 0)], format='xyb')
# msp.add_lwpolyline([(0, 10, 0), (10, 10, .5), (20, 10, 0)], format='xyb')
# my_line_types = [
#     ("DOTTED", "Dotted .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .", [0.2, 0.0, -0.2]),
#     ("DOTTEDX2", "Dotted (2x) .    .    .    .    .    .    .    . ", [0.4, 0.0, -0.4]),
#     ("DOTTED2", "Dotted (.5) . . . . . . . . . . . . . . . . . . . ", [0.1, 0.0, -0.1]),
# ]
# for name, desc, pattern in my_line_types:
#     if name not in doc.linetypes:
#         doc.linetypes.new(name=name, dxfattribs={'description': desc, 'pattern': pattern})
def create_solid_polyline_hatch():
    dwg = ezdxf.new("Ac1024")  # create a new DXF drawing (AutoCAD 2010)
    msp = dwg.modelspace()  # we are working in model space
    hatch = msp.add_hatch(color=2)  # by default a SOLID fill
    # with hatch.edit_boundary() as editor:  # get boundary editor as context object
    hatch.paths.add_polyline_path([(0, 0), (0, 3), (3, 6), (6, 6), (6, 3), (3, 0)])
    dwg.saveas("hatch_solid_polyline.dxf")  # save DXF drawing


def using_hatch_style_with_edge_path():
    def add_edge_path(path_editor, vertices):
        path = path_editor.add_edge_path()  # create a new edge path
        first_point = next(vertices)  # store first point for closing path
        last_point = first_point
        for next_point in vertices:
            path.add_line(last_point, next_point)  # add lines to edge path
            last_point = next_point
        path.add_line(last_point, first_point)  # close path

    def place_square_1(hatch, x, y):
        def shift(point):
            return x+point[0], y+point[1]

        with hatch.edit_boundary() as editor:  # get boundary editor as context object
            add_edge_path(editor, map(shift, [(0, 0), (0, 8), (8, 8), (0, 8)]))  # 1. path
            add_edge_path(editor, map(shift, [(2, 2), (7, 2), (7, 7), (2, 7)]))  # 2. path
            add_edge_path(editor, map(shift, [(4, 4), (6, 4), (6, 6), (4, 6)]))  # 3. path

    def place_square_2(hatch, x, y):
        def shift(point):
            return x+point[0], y+point[1]

        with hatch.edit_boundary() as editor:  # get boundary editor as context object
            add_edge_path(editor, map(shift, [(0, 0), (0, 8), (8, 8), (0, 8)]))  # 1. path
            add_edge_path(editor, map(shift, [(3, 1), (7, 1), (7, 5), (3, 5)]))  # 2. path
            add_edge_path(editor, map(shift, [(1, 3), (5, 3), (5, 7), (1, 7)]))  # 3. path

    dwg = ezdxf.new("Ac1024")  # create a new DXF drawing (AutoCAD 2010)
    msp = dwg.modelspace()  # we are working in model space
    # first create DXF hatch entities
    hatch_style_0 = msp.add_hatch(color=3, dxfattribs={'hatch_style': 0})
    hatch_style_1 = msp.add_hatch(color=3, dxfattribs={'hatch_style': 1})
    hatch_style_2 = msp.add_hatch(color=3, dxfattribs={'hatch_style': 2})
    # then insert path elements to define the hatch boundaries
    place_square_1(hatch_style_0, 0, 0)
    place_square_1(hatch_style_1, 10, 0)
    place_square_1(hatch_style_2, 20, 0)

    # first create DXF hatch entities
    hatch_style_0b = msp.add_hatch(color=7, dxfattribs={'hatch_style': 0})
    hatch_style_1b = msp.add_hatch(color=7, dxfattribs={'hatch_style': 1})
    hatch_style_2b = msp.add_hatch(color=7, dxfattribs={'hatch_style': 2})
    # then insert path elements to define the hatch boundaries
    place_square_2(hatch_style_0b, 0, 10)
    place_square_2(hatch_style_1b, 10, 10)
    place_square_2(hatch_style_2b, 20, 10)
    dwg.saveas("hatch_styles_examples_with_edge_path.dxf")  # save DXF drawing

def create_group():
    dwg = ezdxf.new('Ac1015')
    msp = dwg.modelspace()
    # create a new unnamed group, in reality they have a name like '*Annnn' and group names have to be unique
    group = dwg.groups.add()
    # group management is implemented as context manager, returning a standard Python list
    with group.edit_data() as g:
        # the group itself is not an entity space, DXF entities has to be placed in model space, paper space
        # or in a block
        g.append(msp.add_line((0, 0), (3, 0)))
        g.append(msp.add_circle((0, 0), radius=2))
    dwg.saveas('group.dxf')

def make_test_drawing(version):
    dwg = ezdxf.new(version)
    modelspace = dwg.modelspace()
    modelspace.add_line((0, 0), (10, 0))
    # modelspace.add_text("TEST", dxfattribs={'layer': 'lay_line'})
    # modelspace.add_polyline2d([(0, 0), (3, 1), (7, 4), (10, 0)], {'layer': 'lay_polyline'})
    # just 3 entities: LINE, TEXT, POLYLINE - VERTEX & SEQEND now linked to the POLYLINE entity, and do not appear
    # in any entity space
    return dwg
def add_line():
    doc = ezdxf.new('R2010')  # create a new DXF R2010 drawing, official DXF version name: 'AC1024'

    msp = doc.modelspace()  # add new entities to the modelspace
    msp.add_line((0, 0), (10, 0))  # add a LINE entity
    doc.saveas('line.dxf')

def add_combinerbox():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()  # add new dimension entities to the modelspace
    msp.add_circle((0.5, 0.5), radius=0.5)  # add a CIRCLE entity, not required
    # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
    # msp.add_line((1, 1), (1, 0))
    points = [(0, 0), (1, 0), (1, 1), (0, 1),(0,0)]
    msp.add_lwpolyline(points)

    # add default radius dimension, measurement text is located outside
    # dim = msp.add_radius_dim(center=(0, 0), radius=3, angle=45, dimstyle='EZ_RADIUS')
    # necessary second step, to create the BLOCK entity with the dimension geometry.
    # dim.render()
    doc.saveas('radius_dimension.dxf')


def add_KWxh():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()  # add new dimension entities to the modelspace
    msp.add_circle((0.5, 0.5), radius=0.5)  # add a CIRCLE entity, not required
    # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
    # msp.add_line((1, 1), (1, 0))
    points = [(0, 0), (1, 0), (1, 1), (0, 1),(0,0)]
    msp.add_lwpolyline(points)
    msp.add_text("KWxh",dxfattribs={
                 'style': 'LiberationSerif',
                 'color':'1',
                 'height': 0.2}).set_pos((0.5, 0.39), align='CENTER')

    # add default radius dimension, measurement text is located outside
    # dim = msp.add_radius_dim(center=(0, 0), radius=3, angle=45, dimstyle='EZ_RADIUS')
    # necessary second step, to create the BLOCK entity with the dimension geometry.
    # dim.render()
    doc.saveas('radius_dimension.dxf')


def add_Measurement():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()  # add new dimension entities to the modelspace
    msp.add_circle((0.5, 0.5), radius=0.45)  # add a CIRCLE entity, not required
    # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
    # msp.add_line((1, 1), (1, 0))
    points = [(0, 0), (1, 0), (1, 1), (0, 1),(0,0)]
    msp.add_lwpolyline(points)
    msp.add_text("M",dxfattribs={
                 'style': 'LiberationSerif',
                 'height': 0.35}).set_pos((0.5, 0.35), align='CENTER')
    # dim = msp.add_aligned_dim(p1=(0.5,0.5), p2=(0.8, 0.8), distance=1)
    # msp.add_linear_dim(base=(3, 2), p1=(3, 0), p2=(6, 0), location=(4, 4)).render()
    msp.add_linear_dim(
        base=(0.2, 0), p1=(0.2, 0.2), p2=(0.5, 0.2),
        override={
            'dimse1': 1,  # suppress first extension line
            'dimse2': 1,  # suppress second extension line
            'dimblk': ezdxf.ARROWS.closed_filled,  # arrows just looks better
        }).render()
    # dim = msp.add_linear_dim(base=(3, 2), p1=(3, 0), p2=(6, 0))
    # dim.set_arrows(blk=ezdxf.ARROWS.closed_filled)
    # dim.set_extline1(disable=True)
    # dim.set_extline2(disable=True)
    # dim = msp.add_linear_dim(base=(3, 2), p1=(3, 0), p2=(6, 0))
    # dim.set_arrow(blk="OPEN_30", size=0.25)
    # dim.render()
    # msp.add_linear_dim(
    #     base=(3, 2), p1=(3, 0), p2=(6, 0),
    #     override={
    #         'dimtsz': 0,  # set tick size to 0 to enable arrow usage
    #         'dimasz': 0.25,  # arrow size in drawing units  # arrow block name
    #     }).render()
    # dim.render()
    # dim.render()
    # add default radius dimension, measurement text is located outside
    # dim = msp.add_radius_dim(center=(0, 0), radius=3, angle=45, dimstyle='EZ_RADIUS')
    # necessary second step, to create the BLOCK entity with the dimension geometry.
    # dim.render()
    doc.saveas('Measurement.dxf')

def add_pv():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()  # add new dimension entities to the modelspace
    # msp.add_circle((0, 0), radius=0.5)  # add a CIRCLE entity, not required
    # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
    # msp.add_line((1, 1), (1, 0))
    # points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1),(0,0)]
    # msp.add_lwpolyline(points)
    # msp.add_line((0, 1), (0.3,0.6))
    # msp.add_line((0.5, 1), (0.3, 0.6))
    # msp.add_line((0.5, 0.5), (0.6, 0.5))
    # msp.add_line((0.9, 0.5), (1, 0.5))
    # msp.add_line((1.3, 0.5), (1.4, 0.5))
    # msp.add_line((1.7, 0.5), (1.8, 0.5))
    # msp.add_line((0.25, -0.2), (0.25, 0))
    # msp.add_line((0.25, -0.6), (0.25, -0.5))
    # msp.add_line((0.25, -1), (0.25, -0.9))
    # msp.add_line((0.25, -1.4), (0.25, -1.3))
    points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
    msp.add_lwpolyline(points)  # the flag symbol as 2D polyline
    msp.add_line((0, 1), (0.3, 0.6))
    msp.add_line((0.5, 1), (0.3, 0.6))
    msp.add_line((0.25, -0.2), (0.25, 0))
    msp.add_line((0.25, -0.6), (0.25, -0.5))
    msp.add_line((0.25, -1), (0.25, -0.9))
    msp.add_line((0.25, 1), (0.25, 1.2))
    msp.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
    msp.add_line((-0.1, 1.25), (0.2, 1.25))
    msp.add_line((-0.1, 1.25), (-0.1, 1.1))
    msp.add_line((-0.08, 1.1), (0.08, 1.1))
    msp.add_line((-0.05, 1.08), (0.05, 1.08))
    msp.add_line((-0.03, 1.05), (0.03, 1.05))
    msp.add_ellipse((0, 1.12), major_axis=(0.1, 0), ratio=0.09)
    msp.add_line((-0.1, 1.12), (-0.2, 1.12))
    msp.add_line((-0.15, 1.12), (-0.15, 1.15))
    msp.add_line((-0.15, 1.15), (-0.12, 1.15))
    msp.add_line((-0.15, 1.15), (-0.18, 1.15))

    doc.saveas('pv.dxf')


def add_inverter():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()  # add new dimension entities to the modelspace
    # msp.add_circle((0, 0), radius=0.5)  # add a CIRCLE entity, not required
    # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
    # msp.add_line((1, 1), (1, 0))
    points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2),(0,0)]
    msp.add_lwpolyline(points)
    msp.add_line((1, 0.2), (0,0))
    msp.add_line((0.3, 0.15),(0.35,0.15))
    msp.add_line((0.3,0.13),(0.35,0.13))
    fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
    msp.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
    msp.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
    # msp.add_spline_control_frame(fit_points, method='chord', dxfattribs={'color': 3})
    # msp.add_line((0.5, 1), (0, 0))
    doc.saveas('inverter.dxf')

def add_polar():
    doc = ezdxf.new('R2000')
    msp = doc.modelspace()

    # point format = (x, y, [start_width, [end_width, [bulge]]])
    # set start_width, end_width to 0 to be ignored (x, y, 0, 0, bulge).

    # msp.add_line((8, 2), (9, 2))
    # msp.add_lwpolyline([ (10, 10, .5), (20, 10, 0)], format='xyb')
    # msp.add_line((15,10),(15,9))
    msp.add_arc(center=(6, 2), radius=2.5, start_angle=270, end_angle=90)
    msp.add_circle(center=(6, 4.5), radius=0.1)
    msp.add_circle(center=(6, -0.5), radius=0.1)
    msp.add_line((8, 2), (9, 2))

    doc.saveas("polar.dxf")

def add_bi_polar():
    doc = ezdxf.new('R2000')
    msp = doc.modelspace()

    # point format = (x, y, [start_width, [end_width, [bulge]]])
    # set start_width, end_width to 0 to be ignored (x, y, 0, 0, bulge).

    # msp.add_lwpolyline([(0, 0, 0), (10, 0, 1), (20, 0, 0)], format='xyb')
    msp.add_arc(center=(6, 2), radius=2.5, start_angle=270, end_angle=90)
    msp.add_circle(center=(6, 4.5), radius=0.1)
    msp.add_circle(center=(6, -0.5), radius=0.1)
    msp.add_line((8, 2), (9, 2))
    msp.add_line((8, 1.8), (9, 1.8))
    doc.saveas("bipolar.dxf")


def add_tri_polar():
    doc = ezdxf.new('R2000')
    msp = doc.modelspace()

    # point format = (x, y, [start_width, [end_width, [bulge]]])
    # set start_width, end_width to 0 to be ignored (x, y, 0, 0, bulge).

    # msp.add_lwpolyline([(0, 0, 0), (10, 0, 1), (20, 0, 0)], format='xyb')
    msp.add_arc(center=(6, 2), radius=2.5, start_angle=270, end_angle=90)
    msp.add_circle(center=(6, 4.5), radius=0.1)
    msp.add_circle(center=(6, -0.5), radius=0.1)
    msp.add_line((8, 2), (9, 2))
    msp.add_line((8, 1.8), (9, 1.8))
    msp.add_line((8, 1.6), (9, 1.6))
    doc.saveas("tripolar.dxf")

def add_spearator():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()  # add new dimension entities to the modelspace
    # msp.add_circle((0, 0), radius=0.5)  # add a CIRCLE entity, not required
    # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
    # msp.add_line((1, 1), (1, 0))
    msp.add_line((0, 1), (0.5,1.3))
    msp.add_line((0.1, 1),(0.6,1.3))
    # msp.add_line((0.5, 1), (0, 0))
    doc.saveas('separator.dxf')

def add_cable():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()  # add new dimension entities to the modelspace
    # msp.add_circle((0, 0), radius=0.5)  # add a CIRCLE entity, not required
    # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
    # msp.add_line((1, 1), (1, 0))
    # msp.aline((0, 1), (0.5,2))
    msp.add_lwpolyline([(0, 1), (0.5,1.5)])
    # msp.add_line((0.1, 1),(0.6,1.3))
    # msp.add_line((0.5, 1), (0, 0))
    doc.saveas('cable.dxf')

def add_separator():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()  # add new dimension entities to the modelspace
    # msp.add_circle((0, 0), radius=0.5)  # add a CIRCLE entity, not required
    # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
    # msp.add_line((1, 1), (1, 0))
    # msp.aline((0, 1), (0.5,2))
    msp.add_line((2, 0),(2.2, 0.1))
    msp.add_line((2, 0), (2.2, 0.1))
    doc.saveas('separator.dxf')


def add_gridconnection():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()  # add new dimension entities to the modelspace
    # msp.add_circle((0, 0), radius=0.5)  # add a CIRCLE entity, not required
    msp.add_lwpolyline([(0, 5), (6, 5), (6, 0), (0, 0), (0, 5)])
    msp.add_line((2.5, 0), (2.5, 5))
    doc.saveas('gridconnection.dxf')


def string59():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_line((0, 0), (1, 0))
    msp.add_line((1.1, 0), (1.95, 0))
    msp.add_line((1, -0.1), (1, 0.1))
    msp.add_line((1.1, -0.1), (1.1, 0.1))
    msp.add_ellipse((2, 0), major_axis=(0.05, 0), ratio=0.2)
    msp.add_text("+",dxfattribs={
                 'height': 0.1}).set_pos((1.15, 0), align='CENTER')
    msp.add_text("+",dxfattribs={
                 'height': 0.1}).set_pos((0.95, 0), align='CENTER')
    doc.saveas('string59.dxf')


def string60():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_line((0, 0), (0.5, 0))
    msp.add_line((0.25, -0.1), (0.25, 0.1))
    msp.add_line((0.29, -0.1), (0.29, 0.1))
    msp.add_line((0.33, -0.1), (0.33, 0.1))
    msp.add_line((0.37, -0.1), (0.37, 0.1))
    msp.add_line((0.37, 0.1), (0.4, 0.1))
    doc.saveas('string60.dxf')


def string69():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_line((1, 0), (1.95, 0))
    msp.add_arc(center=(2,0), radius=0.05, start_angle=270, end_angle=265)
    doc.saveas('string69.dxf')


def string120():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_line((1, 0), (1.2, 0.5))
    msp.add_arc(center=(1,0), radius=0.002, start_angle=270, end_angle=265)
    msp.add_arc(center=(1.05, 0.5), radius=0.02, start_angle=0, end_angle=180)
    doc.saveas('string120.dxf')


def RBOX():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_lwpolyline([(0, 0.5), (0.2, 0.5),(0.2, 0), (0, 0), (0, 0.5)]),
    msp.add_line((-0.2, 0.3), (1.5, 0.5))
    msp.add_line((-0.2, 0.3), (-0.2, 0.25))
    msp.add_line((0.1, 0), (0.1, -0.2))
    msp.add_line((0.02, -0.2), (0.2, -0.2))
    msp.add_line((0.05, -0.21), (0.15, -0.21))
    msp.add_line((0.08, -0.22), (0.1, -0.22))
    # msp.add_ellipse((0, 0), major_axis=(0.5, 0), ratio=0.05)
    doc.saveas('RBOX.dxf')


def string99():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_line((2, -0.05), (2.2, -0.05))
    msp.add_line((2.1, 0.2), (2.1, -0.05))
    msp.add_line((2.05, 0.2),(2.15, 0.2))
    msp.add_arc(center=(2,0), radius=0.05, start_angle=270, end_angle=265)
    doc.saveas('string99.dxf')


def string100():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_line((2, 1),(2.2, 1))
    msp.add_line((2.1, 0.9), (2.1, 1.09))
    msp.add_line((2.05, 1.09), (2.15, 1.09))
    msp.add_line((2.045, 0.9),(2.045, 1.09))
    msp.add_line((2.03, 0.9), (2.03, 1.09))
    doc.saveas('string100.dxf')


def add_INVERSOR():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_lwpolyline([(0, 8), (12, 8),(12, 0),(0, 0),(0, 8), (12, 8),(0, 0)]),
    msp.add_line((9, 3), (10, 3)),
    msp.add_line((9, 2.9), (10, 2.9))
    msp.add_arc((2, 6), radius=0.5, start_angle=0, end_angle=180)
    msp.add_arc((3, 6), radius=0.5, start_angle=180, end_angle=0)
    msp.add_arc((2, 5.6), radius=0.5, start_angle=0, end_angle=180)
    msp.add_arc((3, 5.6), radius=0.5, start_angle=180, end_angle=0)
    doc.saveas('INVERSOR.dxf')


def add_stringbox():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_lwpolyline([(0, 8), (12, 8),(12, 0),(0, 0),(0, 8), (12, 8),(0, 0)]),
    msp.add_arc((6, 6), radius=0.5, start_angle=180, end_angle=0)
    msp.add_arc((5, 6), radius=0.5, start_angle=0, end_angle=180)
    msp.add_arc((6, 5.6), radius=0.5, start_angle=180, end_angle=0)
    msp.add_arc((5, 5.6), radius=0.5, start_angle=0, end_angle=180)
    doc.saveas('Stringbox.dxf')

def add_powergeneration():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_lwpolyline([(0, 7), (10, 7), (10, 3), (0, 3),(0, 7)]),
    msp.add_lwpolyline([(1, 6),(3, 6),(3, 5.5), (1, 5.5),(1, 6)]),
    msp.add_line((3, 6), (2.5, 5.8)),
    msp.add_line((3, 5.5),(2.5, 5.8))
    msp.add_lwpolyline([(3.5, 6), (5.5, 6),(5.5, 5.5), (3.5, 5.5),
                        (3.5, 6)]),
    msp.add_line((5.5, 6),(5, 5.8)),
    msp.add_line((5.5, 5.5), (5, 5.8))
    msp.add_lwpolyline([(6, 6),(8, 6), (8, 5.5), (6, 5.5),
                        (6, 6)]),
    msp.add_line((8, 6), (7.5, 5.8)),
    msp.add_line((8, 5.5),(7.5, 5.8))
    msp.add_lwpolyline([(1, 5), (3, 5), (3, 4.5), (1, 4.5),
                        (1, 5)]),
    msp.add_line((3, 5), (2.5, 4.7)),
    msp.add_line((3, 4.5), (2.5, 4.7))
    msp.add_lwpolyline([(3.5, 5), (5.5, 5), (5.5, 4.5), (3.5, 4.5),
                        (3.5, 5)]),
    msp.add_line((5.5, 5),(5, 4.7)),
    msp.add_line((5.5, 4.5), (5, 4.7))
    msp.add_lwpolyline([(6, 5), (8, 5), (8, 4.5), (6, 4.5),
                        (6, 5)]),
    msp.add_line((8, 5), (7.5, 4.7)),
    msp.add_line((8, 4.5), (7.5, 4.7))
    doc.saveas('powergeneration.dxf')

def add_arrow():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_line((1, 0), (1, 1)),
    msp.add_line((1, 1), (0, 0.5)),
    msp.add_line((0, 0.5), (1,0)),
    msp.add_line((1, 0.5), (2, 0.5)),
    msp.add_line((2, 0), (2,1)),
    msp.add_line((2, 0), (2.5, 0.5)),
    msp.add_line((2, 1), (2.5, 0.5)),
    # msp.add_line((-0.2, -0.2), (0.2, 0.2)),
    doc.saveas('arrow.dxf')

def new_RBOX():
    doc = ezdxf.new('R2010', setup=True)
    msp = doc.modelspace()
    msp.add_lwpolyline([(0, 0.5), (0.2, 0.5), (0.2, 0), (0, 0), (0, 0.5)]),
    msp.add_line((-0.2, 0.3), (1.5, 0.5))
    msp.add_line((-0.2, 0.3), (-0.2, 0.25))
    msp.add_line((0.1, 0), (0.1, -0.2))
    msp.add_line((0.02, -0.2), (0.2, -0.2))
    msp.add_line((0.05, -0.23), (0.15, -0.23))
    msp.add_line((0.08, -0.25), (0.1, -0.25))
    msp.add_ellipse((0.1, -0.1), major_axis=(0.1, 0), ratio=0.2)
    msp.add_line((0, -0.1), (-0.2, -0.1))
    msp.add_line((-0.1, -0.1), (-0.1, 0))
    msp.add_line((-0.13, 0), (-0.069, 0))
    doc.saveas('newROBX.dxf')





# print(create_solid_polyline_hatch())
# print(create_group())
# print(make_test_drawing('AC1015'))
# print(add_line())
print(add_pv())
print(add_inverter())
print(add_polar())
print(add_bi_polar())
print(add_tri_polar())
print(add_spearator())
print(add_cable())
print(add_combinerbox())
print(add_gridconnection())
print(string59())
print(string60())
print(string69())
print(string99())
print(string100())
print(add_INVERSOR())
print(add_stringbox())
print(add_powergeneration())
print(add_Measurement())
print(string120())
print(RBOX())
print(add_arrow())
print(new_RBOX())
# doc.saveas("polyline_path.dxf")
